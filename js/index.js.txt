// Boardlist auto refresh code
var boardlist;
var blInterval = 2; // Start updating once in this many timer loops
var replacenode = 'so2-index';

function blUpdate() {
    if ( boardlist.readyState == 4 && boardlist.status == 200 ) {
        var update = boardlist.responseXML.getElementById(replacenode);
        var boards = document.getElementById(replacenode);
        document.getElementById(replacenode).parentNode.replaceChild(update, boards);
    }
}

// Get boardlist every so often
timers.things.push(
    function() {
        if ( timers.loop % blInterval == 0 ) {
            blInterval <<= 1; // double delay every update to prevent flooding the server
            boardlist = new XMLHttpRequest();
            boardlist.onreadystatechange = blUpdate;
            boardlist.open('GET', 'index', true);
            boardlist.send(null);
        }
    }
)
