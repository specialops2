var showAdv;
function advBox() {
    showAdv = document.getElementsByName('enable')[0];
    showAdv.addEventListener('change', toggleAdvanced, false);
    toggleAdvanced();
}

function toggleAdvanced() {
    document.getElementById('advanced').style.display = showAdv.checked ? 'block' : 'none';
}

window.addEventListener('load', advBox, false);
