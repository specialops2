// Container for timed events
var timers = {
    rate: 4 * 1000,      // default loop delay in ms
    loop: 0,             // Count how many times it's looped
    things: Array(),     // functions to be called go into this array
    update: function() { // calls all of the array functions
        for ( var i = 0, j = timers.things.length; i < j; i++ ) {
            timers.things[i]();
        }
        if ( timers.things.length ) { // Don't bother if there's nothing to run
            setTimeout(timers.update, timers.rate);
        }
        timers.loop++;
    }
}

// Start the event timer
function startTimer() {
    setTimeout(timers.update, timers.rate);
}
window.addEventListener('load', startTimer, false);
