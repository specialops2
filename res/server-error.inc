<?php
/* This is for the rare cases where we can't even get the normal error handler set up */
header('HTTP/1.1 500 Internal Server Error');
header('Content-Type: application/xhtml+xml');
set_exception_handler('balls');

function balls($e)
{ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB">
<head>
<title>Error</title>
<link rel="stylesheet" href="css/default" type="text/css"/>
</head>
<body>
<h1>Server Error</h1>
<p><?php echo $e->getMessage() ?></p>
</body>
</html>
<?php exit(1); } ?>
