<?php
require 'con.php';
SO2::$Page->pageheader();
?>

<pre>
<?php
$mtree = array();
$m = SO2::$DB->query('SELECT topicid, messageid, mtext FROM message_data NATURAL LEFT JOIN messages ORDER BY messageid ASC')->fetchAll(PDO::FETCH_ASSOC);
foreach ( $m as $r )
    $mtree[array_shift($r)][] = $r;

foreach ( $mtree as $topicid => $messages ) {
    echo "\nTopic $topicid\n";
    mkdir('data/messages/'.$topicid);
    foreach ( $messages as $msg ) {
        echo $msg['messageid'].' ';
        file_put_contents('data/messages/'.$topicid.'/'.$msg['messageid'], $msg['mtext']);
    }
}
?>
</pre>
