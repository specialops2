<?php
/**
 * Message List
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';

SO2::$Page->title = 'Message List';
SO2::$Page->cacheable = true;

// Get the topic ID
if ( is_numeric($_SERVER['QUERY_STRING']) ) {
    $topicid = $_SERVER['QUERY_STRING'];
} elseif ( isset($_GET['topic']) ) {
    $topicid = $_GET['topic'];
}

// Get topic/board metadata
if ( isset($topicid) && is_numeric($topicid) ) {
    $topic = SO2::$DB->q('SELECT board_name, boards.boardid AS boardid, topic_title, view_lvl, post_lvl, points '.
                         'FROM boards LEFT JOIN topics USING(boardid) WHERE topicid = ?', $topicid);
}

// Check whether the topic exists
if ( empty($topic) ) {
    SO2::$Page->message(Page::ERR_NOTOPIC);
} else {
    // first message in topic is the reply-to message ID for the quickpost and post page link in the navbar
    list($firstmsg, SO2::$Page->mtime) = SO2::$DB->q('SELECT MIN(messageid), MAX(mtime) FROM messages WHERE topicid = ?',
                                                $topicid, SO2_PDO::QROW);
}

if ( ! SO2::$User->has_access('viewboard', $topic) ) { // Check whether or not they can view this board
    SO2::$Page->message(Page::ERR_ULEVEL);
}

// Only add these links after it's known they can access them
SO2::$Page->title .= ': '.$topic['topic_title'];
SO2::$Page->nav['Topic List: '.$topic['board_name']] = 'topiclist?'.$topic['boardid'];
SO2::$Page->nav['Reply'] = 'post?message='.$firstmsg;
SO2::$Page->pageheader();

// Calculate number of messages and starting offset for the message list
$mpp = isset($_GET['length']) ? min(100, $_GET['length']) : SO2::$User->msgs_page;
$currentpage = isset($_GET['page']) ? intval($_GET['page']) : 0;

// Required for message permalinks to work
define('HERE', 'messagelist?topic='.$topicid.';page='.$currentpage.';length='.$mpp);

// Load the message list
if ( file_exists('lib/Messagestyle_'.SO2::$User->msglist_style.'.php') ) {
    $style = 'Messagestyle_'.SO2::$User->msglist_style;
} else {
    $style = 'Messagestyle_Default';
}

if ( file_exists('lib/Messagelist_'.SO2::$User->msglist_layout.'.php') ) {
    $tmp = 'Messagelist_'.SO2::$User->msglist_layout;
    $mlist = new $tmp($topicid, new $style, $mpp, $currentpage);
} else {
    SO2::$User->msglist_layout = 'Flat';
    $mlist = new Messagelist_Flat($topicid, new $style, $mpp, $currentpage);
}

// Display the message list
if ( $l = $mlist->pagelist() ) {
    echo '<dl id="pagelist-head" class="nl"><dt>Pages:</dt> ',$l,"</dl>\n";
}
echo '<div id="messagelist" class="',get_class($mlist)," $style\">\n";
    $mlist->display();
echo "</div>\n";
if ( $l ) {
    echo '<dl id="pagelist-foot" class="nl"><dt>Pages:</dt> '.$l."</dl>\n";
}

// Display the quickpost box if enabled
if ( SO2::$User->getopt('quickpost') ) {
?>
<form action="post?message=<?php echo $firstmsg ?>" method="post" id="quickpost">
  <fieldset>
    <legend>Quick Reply</legend>
    <textarea rows="5" cols="60" name="message_text" tabindex="1"><?php
        if ( SO2::$User->sig ) {
            echo ( strpos($_SERVER['HTTP_USER_AGENT'], 'KHTML') ? "\n\n" : "\n" ),htmlspecialchars(SO2::$User->sig);
        }
    ?></textarea>
    <button type="submit" accesskey="p" name="post" tabindex="1">Post (P)</button>
    <button type="submit" accesskey="r" name="preview" tabindex="1">Preview (R)</button>
  </fieldset>
</form>
<?php
}
?>
