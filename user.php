<?php
/**
 * Userinfo Page
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';
SO2::$Page->title = 'User Details';

$uid = is_numeric($_SERVER['QUERY_STRING']) ? intval($_SERVER['QUERY_STRING']) : SO2::$User->userid;

if ( SO2::$User->userid != $uid && ! SO2::$DB->q('SELECT COUNT(*) FROM users WHERE userid = ?', $uid, SO2_PDO::QVALUE) ) {
    SO2::$Page->message(Page::ERR_NOUSER);
}

if ( SO2::$User->userid == $uid ) { // Own details
    if ( ! (SO2::$User instanceof User_Authenticated) ) {
        SO2::$Page->message(Page::ERR_LOGIN);
    }
    SO2::$Page->title = 'Your Details';
    SO2::$Page->usernav = array_merge(SO2::$Page->usernav, array( // Add usermenus
                        'General Options' => 'options',
                        'Customise Theme' => 'theme',
                        'Change Password' => 'passwd'
                       ) );
    
    if ( 0 < SO2::$User->points ) {
        SO2::$Page->usernav = array_merge(SO2::$Page->usernav, array( // Add more usermenus
                        'User Directory' => 'userlist',
                        'Invites' => 'invites'
                       ) );
    }
    SO2::$Page->pageheader();
    $user2 = SO2::$User;
} else { // Whois page
    $user2 = new User_Registered($uid);
    SO2::$Page->title = 'User Details for '.$user2;
    SO2::$Page->pageheader();
}

// Userlevel descriptions
$userlevels = array (
    'none'   => 'Suspended',
    'normal' => 'Normal account',
    'vip'    => 'VIP',
    'admin'  => 'Admin',
);

// Details shown to anyone, logged in or not
$fields = array (
    'Account ID' => $user2->userid,
    'Points'     => $user2->points,
    'User Level' => $userlevels[$user2->level],
    'Mod Level'  => $user2->has_access('moderate'),
);

if ( SO2::$User->points > 0 ) {
    $sig = new Post_Plaintext($user2->sig);
    $quote = new Post_Plaintext($user2->quote);
    try {
        $contact = new Post_Default($user2->public_contact);
    } catch ( Exception $e ) {
        $contact = new Post_Plaintext($user2->public_contact);
    }

    $fields = array_merge($fields, array (
    'Posts'             => $user2->posts,
    'Registration Date' => SO2::$Page->fdate($user2->register_date),
    'Invited By'        => ($user2->referrer ? SO2::$Page->namelink($user2->referrer) : 'N/A'),
    'Last Active At'    => SO2::$Page->fdate($user2->last_active_date),
    'Timezone'          => $user2->tz,
    'Signature'         => $sig->getOutput(),
    'Quote'             => $quote->getOutput(),
    'Public Contact Address' => $contact->getOutput(),
    'Browser'           => htmlspecialchars($user2->useragent),
    ) );
}

if ( SO2::$User->admin ) {
    $fields = array_merge($fields, array (
    'Private Contact Address' => htmlspecialchars($user2->private_contact),
    'Signup Contact Address'  => htmlspecialchars($user2->reg_contact),
    'Last Seen Online'        => SO2::$Page->fdate($user2->last_passive_date),
    'Last Login From'         => $user2->last_login_ip,
    'Last Active From'        => $user2->last_ip,
    'Signed Up From'          => $user2->reg_ip,
    ) );
};
?>

<table>
  <caption>User Details for <strong><?php echo $user2 ?></strong></caption>
  <tbody><?php $a = 1; foreach ( $fields as $key => $value ) {
      echo '  <tr><th scope="row">',$key,'</th><td class="content c',(++$a&1),'">',$value,"</td></tr>\n";
  } ?></tbody>
</table>

<?php
if ( 0 == SO2::$User->points ) {
    SO2::$Page->message('Only registered active accounts can view full profile information.', E_USER_NOTICE);
}
?>
