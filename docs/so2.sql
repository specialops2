-- phpMyAdmin SQL Dump
-- version 2.10.0.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Mar 26, 2007 at 11:43 PM
-- Server version: 5.0.34
-- PHP Version: 5.2.1-pl3-gentoo

SET FOREIGN_KEY_CHECKS=0;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

SET AUTOCOMMIT=0;
START TRANSACTION;

-- 
-- Database: 'so2dev'
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table 'boards'
-- 

CREATE TABLE boards (
  boardid smallint(6) NOT NULL auto_increment,
  groupid tinyint(3) unsigned NOT NULL default '0',
  board_name varchar(255) NOT NULL,
  caption varchar(255) NOT NULL,
  post_lvl enum('none','points','vip','admin') NOT NULL default 'none',
  topic_lvl enum('none','points','vip','admin') NOT NULL default 'none',
  view_lvl enum('none','login','points','vip','admin') NOT NULL default 'none',
  points int(11) NOT NULL default '0',
  posts int(10) unsigned NOT NULL default '0',
  topics int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (boardid),
  KEY groupid (groupid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table 'board_groups'
-- 

CREATE TABLE board_groups (
  groupid tinyint(3) unsigned NOT NULL auto_increment,
  group_name varchar(50) character set latin1 NOT NULL,
  hidden tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (groupid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table 'marks'
-- 

CREATE TABLE marks (
  messageid mediumint(8) unsigned NOT NULL default '0',
  userid smallint(5) unsigned NOT NULL,
  `change` tinyint(4) NOT NULL,
  `time` int(10) unsigned NOT NULL,
  PRIMARY KEY  (messageid,userid)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table 'messages'
-- 

CREATE TABLE messages (
  messageid int(10) unsigned NOT NULL auto_increment,
  topicid int(10) unsigned NOT NULL default '0',
  userid smallint(5) unsigned NOT NULL default '0',
  mtime int(10) unsigned NOT NULL,
  score smallint(6) NOT NULL default '0',
  marks smallint(5) unsigned NOT NULL default '0',
  replyto int(10) unsigned default NULL,
  origin_ip int(10) unsigned NOT NULL,
  PRIMARY KEY  (messageid),
  KEY topicid (topicid),
  KEY userid (userid),
  KEY replyto (replyto)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table 'themes'
-- 

CREATE TABLE themes (
  themeid tinyint(3) unsigned NOT NULL auto_increment,
  theme_name char(20) NOT NULL,
  css_file char(20) NOT NULL,
  PRIMARY KEY  (themeid)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 PACK_KEYS=0 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

-- 
-- Table structure for table 'things'
-- 

CREATE TABLE things (
  id smallint(5) unsigned NOT NULL auto_increment,
  userid smallint(5) unsigned NOT NULL,
  what enum('invite','admin') NOT NULL,
  `data` varchar(36) default NULL,
  PRIMARY KEY  (id),
  KEY userid (userid),
  KEY item (what)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table 'topics'
-- 

CREATE TABLE topics (
  topicid int(10) unsigned NOT NULL auto_increment,
  topic_title varchar(255) NOT NULL,
  userid smallint(5) unsigned NOT NULL default '0',
  boardid smallint(6) NOT NULL default '0',
  visibility enum('normal','sticky','deleted') NOT NULL default 'normal',
  closed tinyint(1) NOT NULL default '0',
  lastpost int(10) unsigned default NULL,
  posts int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (topicid),
  KEY lastpost (lastpost),
  KEY boardid (boardid),
  KEY userid (userid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

-- 
-- Table structure for table 'users'
-- 

CREATE TABLE users (
  userid smallint(5) unsigned NOT NULL auto_increment,
  alias varchar(25) NOT NULL,
  passwd tinyblob,
  `level` enum('none','normal','vip','admin') NOT NULL default 'normal',
  points int(10) NOT NULL default '0',
  cutoff int(11) NOT NULL default '0',
  invites tinyint(3) unsigned NOT NULL default '0',
  referrer smallint(5) unsigned default NULL,
  options set('alwaysonline','javascript','cache','quickpost') NOT NULL default 'alwaysonline',
  boardlist_layout varchar(16) NOT NULL,
  topiclist_layout varchar(16) NOT NULL,
  msglist_layout varchar(16) NOT NULL,
  msglist_style varchar(16) NOT NULL,
  topics_page tinyint(3) unsigned NOT NULL default '35',
  msgs_page tinyint(3) unsigned NOT NULL default '35',
  post_html varchar(16) NOT NULL default 'Default',
  tz varchar(24) NOT NULL default 'UTC',
  theme_type enum('system','user') NOT NULL default 'system',
  theme_id smallint(5) unsigned NOT NULL default '1',
  custom_css tinyint(1) NOT NULL default '0',
  last_passive_date int(10) unsigned NOT NULL,
  last_active_date int(10) unsigned NOT NULL,
  register_date int(10) unsigned NOT NULL,
  sig varchar(255) default NULL,
  quote varchar(255) default NULL,
  useragent varchar(160) default NULL,
  public_contact varchar(100) default NULL,
  private_contact varchar(100) default NULL,
  reg_contact varchar(100) NOT NULL,
  last_ip int(10) unsigned default '0',
  last_login_ip int(10) unsigned default '0',
  reg_ip int(10) unsigned default '0',
  date_format varchar(20) NOT NULL default 'Y-m-d H:i:s',
  PRIMARY KEY  (userid),
  KEY u_referrer (referrer),
  KEY alias (alias),
  KEY last_active_date (last_active_date)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 
-- Constraints for dumped tables
-- 

-- 
-- Constraints for table `messages`
-- 
ALTER TABLE `messages`
  ADD CONSTRAINT messages_ibfk_12 FOREIGN KEY (topicid) REFERENCES topics (topicid) ON DELETE CASCADE,
  ADD CONSTRAINT messages_ibfk_13 FOREIGN KEY (userid) REFERENCES users (userid) ON DELETE CASCADE,
  ADD CONSTRAINT messages_ibfk_14 FOREIGN KEY (replyto) REFERENCES messages (messageid) ON DELETE CASCADE;

-- 
-- Constraints for table `things`
-- 
ALTER TABLE `things`
  ADD CONSTRAINT things_ibfk_1 FOREIGN KEY (userid) REFERENCES users (userid) ON DELETE CASCADE;

-- 
-- Constraints for table `topics`
-- 
ALTER TABLE `topics`
  ADD CONSTRAINT topics_ibfk_3 FOREIGN KEY (userid) REFERENCES users (userid) ON DELETE CASCADE,
  ADD CONSTRAINT topics_ibfk_4 FOREIGN KEY (lastpost) REFERENCES messages (messageid) ON DELETE NO ACTION;

-- 
-- Constraints for table `users`
-- 
ALTER TABLE `users`
  ADD CONSTRAINT users_ibfk_1 FOREIGN KEY (referrer) REFERENCES users (userid) ON DELETE CASCADE;

SET FOREIGN_KEY_CHECKS=1;

COMMIT;
