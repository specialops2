-- phpMyAdmin SQL Dump
-- version 2.10.0.2
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Mar 12, 2007 at 01:00 AM
-- Server version: 5.0.34
-- PHP Version: 5.2.1-pl3-gentoo

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

SET AUTOCOMMIT=0;
START TRANSACTION;

-- 
-- Database: `so2dev`
-- 

-- 
-- Dumping data for table `themes`
-- 

INSERT INTO `themes` (`themeid`, `theme_name`, `css_file`) VALUES 
(1, '(Site Default)', 'default'),
(2, 'GameFAQs Classic', 'gamefaqs'),
(3, 'Scalyfux', 'scaly'),
(4, 'GameFAQs Hell II', 'gfh2'),
(5, 'B-Type', 'b-type'),
(6, 'Frozen Midnight 2', 'fm'),
(7, 'GFCSS 10', 'gfcss10'),
(8, 'BlueWind III', 'bw3'),
(9, 'BloodWind III', 'bw3-inv');

COMMIT;
