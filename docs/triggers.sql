CREATE TRIGGER upd_topics AFTER INSERT ON messages
FOR EACH ROW UPDATE topics SET
    lastpost = NEW.messageid,
    posts = (SELECT COUNT(messageid) FROM messages WHERE topicid = NEW.topicid)
WHERE topics.topicid = NEW.topicid;

CREATE TRIGGER upd_boardposts AFTER UPDATE ON topics
FOR EACH ROW UPDATE boards SET
    posts = (SELECT SUM(posts) FROM topics WHERE boardid = NEW.boardid)
WHERE boards.boardid = NEW.boardid;

CREATE TRIGGER upd_boardtopics AFTER INSERT ON topics
FOR EACH ROW UPDATE boards SET
    topics = (SELECT COUNT(*) FROM topics WHERE boardid = NEW.boardid)
WHERE boards.boardid = NEW.boardid;
