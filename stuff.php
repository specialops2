<?php
/**
 * Statistics, FAQ & TOS
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';

SO2::$Page->title = 'The other stuff';
SO2::$Page->pageheader();

// Stats columns
$date = array (
    'Today'         => mktime(0, 0, 0),
    'This week'     => mktime(0, 0, 0) - date('w')*86400,
    'This month'    => mktime(0, 0, 0, date('m'), 1),
    'Last 24 hours' => (T_NOW - 86400),
    'Last 7 days'   => (T_NOW - 86400*7),
    'Last 30 days'  => (T_NOW - 86400*30),
    'Total'         => 0
);

// Stats rows
$table = array (
    'All Posts'        => 'SELECT COUNT(*) FROM messages WHERE mtime > ?',
    'Replies'          => 'SELECT COUNT(*) FROM messages WHERE replyto IS NOT NULL AND mtime > ?',
    'Topics'           => 'SELECT COUNT(DISTINCT(topicid)) FROM messages WHERE mtime > ?',
    'Registered Users' => 'SELECT COUNT(*) FROM users WHERE register_date > ?',
    'Online Users'     => 'SELECT COUNT(userid) FROM users WHERE last_active_date > register_date AND last_active_date > ?',
    'Active Users'     => 'SELECT COUNT(DISTINCT(users.userid)) FROM users NATURAL LEFT JOIN messages WHERE messages.mtime > ?'
);
?>

<table id="board_stats">
  <caption>Board Statistics</caption>
  <colgroup title="Row headers">
    <col class="num"/>
  </colgroup>
  <colgroup title="Fuzzy statistics">
    <col class="num"/>
    <col class="num"/>
    <col class="num"/>
  </colgroup>
  <colgroup title="Raw statistics">
    <col class="num"/>
    <col class="num"/>
    <col class="num"/>
    <col class="num"/>
  </colgroup>
  <thead>
    <tr>
      <td/>
<?php foreach ( $date as $th => $time ) echo "      <th>$th</th>\n"; ?>
    </tr>
  </thead>
  <tbody>
<?php
$a = 1;

foreach ( $table as $rowth => $sql ) {
    $q = SO2::$DB->prepare($sql);
    echo '    <tr class="content c',(++$a&1),"\">\n",
         '      <th scope="row">',$rowth,"</th>\n";
    foreach ($date as $num) {
        $q->execute( array($num) );
        echo
         '      <td>'.$q->fetchColumn(0)."</td>\n";
    }
    $q = null;
    echo "    </tr>\n";
}
?>
  </tbody>
</table>
<hr/>
<?php readfile('res/faq.xml') ?>
<hr/>
<?php readfile('res/tos.xml') ?>
