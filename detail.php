<?php
/**
 * Message Detail page
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @licence file://COPYING
 * @version 2.15
 */

require 'con.php';

SO2::$Page->title = 'Message Detail';

// Error checks
if ( ! (SO2::$User instanceof User_Authenticated) ) {
    SO2::$Page->message(Page::ERR_LOGIN);
}
if ( ! SO2::$User->has_access('moderate') ) {
    SO2::$Page->message(Page::ERR_ULEVEL);
}
if ( empty($_SERVER['QUERY_STRING']) || !is_numeric($_SERVER['QUERY_STRING']) ) {
    SO2::$Page->message(Page::ERR_NOMSG);
}
if ( isset($_POST['action']) && !in_array($_POST['action'], array('add', 'sub')) ) {
    SO2::$Page->message(Page::ERR_BADREQ);
}


// Get topic metadata
$topic = SO2::$DB->q('SELECT board_name, boards.boardid, topic_title, view_lvl, points, topicid '.
                     'FROM boards LEFT JOIN topics USING(boardid) '.
                     'WHERE topicid = (SELECT topicid FROM messages WHERE messageid = ?)',
                     $_SERVER['QUERY_STRING']);

// more error checks plz
if ( ! $topic ) {
    SO2::$Page->message(Page::ERR_NOMSG);
}

if ( ! SO2::$User->has_access('viewboard', $topic) ) {
    SO2::$Page->message(Page::ERR_ULEVEL);
}


// Set header stuff
SO2::$Page->title .= sprintf(': %s (msg#%d)', $topic['topic_title'], $_SERVER['QUERY_STRING']);
SO2::$Page->nav['Topic List: '.$topic['board_name']] = 'topiclist?'.$topic['boardid'];
SO2::$Page->nav['Message List: '.$topic['topic_title']] = 'messagelist?'.$topic['topicid'];


// Get metadata from the actual message being modified
$meta = SO2::$DB->q('SELECT userid, mtime, topicid, replyto, score, marks, messageid, INET_NTOA(origin_ip) AS ip '.
                    'FROM messages WHERE messageid = ?', $_SERVER['QUERY_STRING']);

define('HERE', $_SERVER['REQUEST_URI']);

if ( isset($_POST['action']) ) {
    
    // Update message with new score
    try {
        SO2::$DB->beginTransaction();

        // Score is proportional to the user's number of points
        $score = ( 'add' === $_POST['action']
                 ?   SO2::$User->has_access('moderate')
                 :-( SO2::$User->has_access('moderate') )
                 );
        
        // Security check
        if ( !isset($_SERVER['HTTP_REFERER'])
        ||   !preg_match('/messagelist|detail/', $_SERVER['HTTP_REFERER'])
        ||   strpos($_SERVER['HTTP_REFERER'], 'http://'.$_SERVER['HTTP_HOST']) !== 0 ) {
            throw new InvalidInputException('Invalid HTTP referrer sent: make sure you\'re using the right links.');
        }

        if ( SO2::$DB->q('SELECT COUNT(*) FROM marks WHERE userid = @userid AND messageid = ?',
                         $_SERVER['QUERY_STRING'], SO2_PDO::QVALUE) ) {
            throw new RateLimitException('You\'ve already marked/suggested this message.');
        }

        if ( SO2::$User->userid === $meta['userid'] ) {
            SO2::$User->points -= 5;
            throw new RateLimitException('No.');
        }
        
        // Update message score
        SO2::$DB->q('UPDATE messages SET score = score + ?, marks = marks + 1 WHERE messageid = ?',
                    array($score, $meta['messageid']));
        
        // Update user score
        $user2 = new User_Registered($meta['userid']);
        $user2->points += $score;
        
        // Add to marked messages list
        SO2::$DB->q('INSERT INTO marks VALUES (?, @userid, ?, UNIX_TIMESTAMP())', array($_SERVER['QUERY_STRING'], $score));
        
        SO2::$DB->commit();
        
        header('Refresh: 5; url='.$_SERVER['HTTP_REFERER']);
        SO2::$Page->message('Message rated! You will be sent back to the previous page in 5 seconds.', E_USER_NOTICE);
        exit;
    } catch ( RateLimitException $e ) {
        SO2::$Page->message($e->getMessage(), E_USER_WARNING);
    } catch ( InvalidInputException $e ) {
        SO2::$Page->message($e->getMessage(), E_USER_WARNING);
    }
} else {
    SO2::$Page->pageheader();
}

if ( file_exists('lib/Messagestyle_'.SO2::$User->msglist_style.'.php') ) {
    $style = 'Messagestyle_'.SO2::$User->msglist_style;
} else {
    $style = 'Messagestyle_Default';
}
$mo = new $style;

echo '<div class="',get_class($mo),"\">\n";
    $mo->display($meta);
echo "</div>\n";

if ( ! isset($_POST) && SO2::$User->userid != $message['userid'] ) {
    echo '<p class="info">Click the +/- links to give or take points. Everyone can see who voted for a message.</p>';
}

$marks = SO2::$DB->q('SELECT userid FROM marks WHERE messageid = ?', $_SERVER['QUERY_STRING'], SO2_PDO::QOBJ)->fetchAll(PDO::FETCH_COLUMN);

if ( count($marks) ) {
    echo '<p class="info">Post modded by: ',implode(', ', array_map(array(SO2::$Page, 'namelink'), $marks)),"</p>\n";
}
?>
