<?php
/**
 * Topic List
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';

SO2::$Page->title = 'Topic List';
SO2::$Page->cacheable = true;

// Get board metadata
$boardid = isset($_GET['board']) ? intval($_GET['board']) : intval($_SERVER['QUERY_STRING']);

$b = new Board($boardid);
$board = $b->get_info();

// Or croak if it doesn't exist
if ( ! $board ) {
    SO2::$Page->message(Page::ERR_NOBOARD);
}

SO2::$Page->title .= ': '.$board['board_name'];

if ( ! SO2::$User->has_access('viewboard', $board) ) { // View restriction
    SO2::$Page->message(Page::ERR_ULEVEL);
}
if ( SO2::$User->has_access('posttopic', $board) ) { // Add topic post link if they're allowed to post one
    SO2::$Page->usernav['Post Topic'] = 'post?board='.$boardid;
}

// Set the page last-modified time
SO2::$Page->mtime = SO2::$DB->query('SELECT MAX(mtime) FROM messages, topics WHERE messageid = lastpost AND boardid = @boardid')
                                ->fetchColumn(0);

SO2::$Page->pageheader();

// Get current page of topics
$start = isset($_GET['page']) ? intval($_GET['page']) : 0;

// Load topiclist module
if ( file_exists('lib/Topiclist_'.SO2::$User->topiclist_layout.'.php') ) {
    $tmp = 'Topiclist_'.SO2::$User->topiclist_layout;
    $tlist = new $tmp(SO2::$User->topics_page, $start);
} else {
    SO2::$User->topiclist_layout = 'Default';
    $tlist = new Topiclist_Default(SO2::$User->topics_page, $start);
}

// Output topiclist
if ( $l = $tlist->pagelist() ) {
    echo '<dl id="pagelist-head" class="nl"><dt>Pages:</dt> '.$l."</dl>\n";
}
echo '<div id="topiclist" class="',get_class($tlist),"\">\n";
    $tlist->display();
echo "</div>\n";
if ( $l ) {
    echo '<dl id="pagelist-foot" class="nl"><dt>Pages:</dt> '.$l."</dl>\n";
}

// Quickpost thing
if ( SO2::$User->has_access('posttopic', $board) && SO2::$User->getopt('quickpost') ) { ?>
<form action="post?board=<?php echo $boardid ?>" method="post" id="quickpost">
  <fieldset>
    <legend>Quick Topic</legend>
    <p><label>
      Title:
      <input type="text" name="topic_title" maxlength="60" size="80" tabindex="1"/>
    </label></p>
    <textarea rows="5" cols="60" name="message_text" tabindex="1"><?php
        if ( SO2::$User->sig ) {
            echo ( strpos($_SERVER['HTTP_USER_AGENT'], 'KHTML') ? "\n\n" : "\n" ),htmlspecialchars(SO2::$User->sig);
        }
    ?></textarea>
    <button type="submit" accesskey="p" name="post" tabindex="1">Post (P)</button>
    <button type="submit" accesskey="r" name="preview" tabindex="1">Preview (R)</button>
  </fieldset>
</form>
<?php } ?>
