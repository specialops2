<?php
/**
 * Default topic list
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.99
 */
class Topiclist_Default extends Topiclist_Lite implements Topiclist
{
    public function display()
    {
        echo
        "<table>\n",
        '<col/><col/><col class="num"/><col/>',"\n",
        "<thead>\n",
        "<tr>\n",
        '  <th scope="col">Topic Name</th>',"\n",
        '  <th scope="col">Created By</th>',"\n",
        '  <th scope="col">Posts</th>',"\n",
        '  <th scope="col">Last Post</th>',"\n",
        "</tr>\n",
        "</thead>\n",
        "<tbody>\n";
        
        // Guaranteed to be O(log n) or your money back!
        $q = SO2::$DB->q(
                'SELECT topics.topicid, topic_title, topics.userid, messageid, mtime, messages.userid AS lastpostuid, posts '.
                'FROM topics LEFT JOIN messages ON lastpost = messageid '.
                'WHERE boardid = @boardid GROUP BY topicid ORDER BY mtime DESC LIMIT ?, ?',
                array($this->page*$this->tpp, $this->tpp), SO2_PDO::QOBJ);
        $topics = $q->fetchAll();
        
        $a = 1;
        
        foreach ( $topics as $topic ) {
            printf(
                '<tr class="content c%d u%d">'."\n".
                '  <td><a class="topic" href="messagelist?%d">%s</a></td>'."\n".
                "  <td>%s</td>\n".
                "  <td>%d</td>\n".
                "  <td><small>%s<br/>By: %s</small></td>\n".
                "</tr>\n",
                    (++$a&1),
                    $topic['userid'],
                    $topic['topicid'],
                    $topic['topic_title'],
                    SO2::$Page->namelink($topic['userid']),
                    $topic['posts'],
                    SO2::$Page->fdate($topic['mtime']),
                    SO2::$Page->namelink($topic['lastpostuid'])
            );
        }
        echo "</tbody>\n</table>\n";
    }
}
?>
