<?php
/**
 * Plaintext message parser - just acts like <xmp/>
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
require_once 'lib/iface.Message3.php';

class Post_Plaintext implements Message3
{
    private $output;
    
    function __construct($input, $formatting = null)
    {   
        $this->output = nl2br(htmlspecialchars($input));
    }
    
    function validate()
    {
        return null;
    }
    
    function getOutput()
    {
        return $this->output;
    }
}
?>
