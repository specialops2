<?php
/**
 * HTML radio button object
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license http://specialops.ath.cx/repos/so2/trunk/COPYING (New BSD Licence)
 * @version 2.15
 */
class HTML_Radiobutton
{
    public $name;
    public $label;
    public $value;
    public $checked;

    function __construct($name, $label, $value = '', $checked = false)
    {
        $this->name = $name;
        $this->label = $label;
        $this->value = $value;
        $this->checked = $checked;
    }

    function is_selected()
    {
        return isset($_POST[$this->name]) && $_POST[$this->name] == $this->value;
    }

    function __toString()
    {
        return '<label>'.
               '<input type="radio" name="'.$this->name.'" value="'.$this->value.'"'.
               ($this->checked ? ' checked="checked"' : '').'/>'.htmlspecialchars($this->label).'</label>';
    }

    static function factory($name, array $buttons, $default = null)
    {
        $ret = array();
        foreach ( $buttons as $value => $label ) {
            $ret[$value] = new HTML_Radiobutton($name, $label, $value, ($value==$default));
        }
        return $ret;
    }
}
?>
