<?php
/**
 * Topiclist class interface
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
interface topiclist
{
    /**
     * Constructor
     * @param int $tpp Number of topics per page
     * @param int $page Page number (starts from 0)
     */
    function __construct($tpp = 35, $page = 0);
    
    /**
     * Echo the topic list. Doesn't return anything.
     * @return null
     */
    public function display();
    
    /**
     * Return (not echo) list of pages in a XHTML dd format (with no dl tags)
     *
     * If the class can't handle pages for whatever reason, then return null instead.
     * @return string|null
     */
    public function pagelist();
}
?>
