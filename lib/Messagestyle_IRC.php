<?php
/**
 * IRC-style messagelist
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
require_once 'lib/Messagestyle_Default.php';

class Messagestyle_IRC extends Messagestyle_Default
{
    protected function msgheader(array $m)
    {
        echo '<a href="',HERE,'#m',$m['messageid'],'">#',$m['messageid'],'</a>',
             '&nbsp;&lt;',SO2::$Page->namelink($m['userid']),"&gt;<br/>\n";
             
        if ( SO2::$User->has_access('moderate') && SO2::$User->userid != $m['userid'] ) {
            echo <<<HTML
<form action="detail?{$m['messageid']}" method="post" class="msgmod">
  <button type="submit" name="action" value="add">+</button>
  <button type="submit" name="action" value="sub">-</button>
</form><br/>
HTML;
        }
        
        echo '(Score: ',$m['score'],' <a href="detail?',$m['messageid'],'">?</a>)<br/>',"\n",
             '(<a href="post?message=',$m['messageid'],'">Reply</a>)';
    }
}
?>
