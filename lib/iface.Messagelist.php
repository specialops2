<?php
/**
 * Messagelist structure interface
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
interface Messagelist
{
    const MODMSG = '<span class="deletedmsg">[Message below your minimum score setting (%d)]</span>';

    /**
     * Get a topic ID, the message list layout, where to start and do something with it all.
     * @param int $topicid Topic ID to get
     * @param msglist $mv Messagelist object to use for output
     * @param int $tpp Number of messages per page, if possible
     * @param int $page Page number (zero index)
     */
    function __construct($topicid, Messagestyle $mo, $mpp, $page);
    
    /**
     * Output the messagelist using the style module passed to the constructor.
     * @return null
     */
    public function display();
    
    /**
     * Return (not echo) list of pages in a XHTML dd format (with no dl tags)
     *
     * If the class can't handle pages for whatever reason, this should return null.
     * @return string|null
     */
    public function pagelist();
}
?>
