<?php
/**
 * Plain messagelist like GF uses
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
require_once 'lib/iface.Messagestyle.php';

class Messagestyle_Default implements Messagestyle
{
    static $lastmsgid;
    
    public final function display(array $m)
    {
        if ( $m['score'] < -10 )
            return;

        echo '<div id="m',$m['messageid'],'" class="message u',$m['userid'],'">',"\n",
             '  <div class="info">'; $this->msgheader($m); echo "</div>\n",
             '  <div class="content">'; $this->msgbody($m); echo "</div>\n",
             "</div>\n";

        self::$lastmsgid = $m['messageid'];
    }

    protected function msgheader(array $m)
    {
        echo 'From: ',SO2::$Page->namelink($m['userid']),
          ' | At: ',SO2::$Page->fdate($m['mtime']),
  sprintf(' | <a href="%s#m%d">#%2$d</a>', HERE, $m['messageid']),
          ' | Score: ',$m['score'],' <a href="detail?',$m['messageid'],'"><img src="res/ternary" alt="Message Detail"/></a>';

        if ( SO2::$User->has_access('moderate') && SO2::$User->userid != $m['userid'] ) {
            echo '<form action="detail?',$m['messageid'],'" method="post" class="msgmod">',"\n",
                 '  <button type="submit" name="action" value="add"><img src="res/plus" alt="[+]"/></button>',"\n",
                 '  <button type="submit" name="action" value="sub"><img src="res/minus" alt="[-]"/></button>',"\n",
                 "</form>\n";
        }
        
        echo ' | <a href="post?message=',$m['messageid'],'">Reply to this</a>';

        // Don't bother adding this if the other message is right above this one
        if ( $m['replyto'] && $m['replyto'] != self::$lastmsgid ) {
            echo sprintf(' | Reply to <a href="#m%d">#%1$d</a>', $m['replyto']);
        }
        if ( SO2::$User->admin ) {
            echo ' | ',$m['ip'];
        }
    }

    protected function msgbody(array $m)
    {
        if ( isset($m['hidden']) )
            echo '<span class="deletedmsg">[This message is hidden due to your display preferences.]</span>';
        elseif ( isset($m['mtext']) )
            echo $m['mtext'];
        elseif ( file_exists('data/messages/'.$m['topicid'].'/'.$m['messageid']) )
            readfile('data/messages/'.$m['topicid'].'/'.$m['messageid']);
        else
            echo '<span class="deletedmsg">[This message was deleted by a ',SO2::$Cfg['site']['title'],' moderator.]</span>';
    }
}
?>
