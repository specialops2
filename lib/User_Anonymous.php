<?php
/**
 * Anonymous user class
 *
 * This is used to represent a user that's not logged in.
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
require_once 'iface.User.php';

class User_Anonymous implements User
{
    protected $attrcache = array (
        'theme' => -1,
        'tz' => 'UTC',
        'date_format' => 'Y-m-d H:i:s',
        'topics_page' => 35,
        'msgs_page' => 35,
    );

    function __get($varname)
    {
        if ( isset($this->attrcache[$varname]) ) {
            return $this->attrcache[$varname];
        }
        return null;
    }
    
    function __toString()
    {
        return '[nobody]';
    }
    
    public function has_access($feature, array $details = array())
    {
        switch ( $feature ) {
            // Board access
            case 'posttopic':
                return ('none' == $details['topic_lvl']);
            case 'postmessage':
                return ('none' == $details['post_lvl']);
            case 'viewboard':
                return ('none' == $details['view_lvl']);

            default:
                return false;
        }
    }

    public function getopt($name)
    {
        if ( 'cache' == $name ) {
            return true;
        } else {
            return null;
        }
    }
}
?>
