<?php
/**
 * SO2 default topic list layout
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */

require_once 'lib/iface.Topiclist.php';

class Topiclist_Lite implements Topiclist
{
    protected $tpp;
    protected $page;
    protected $topicsum;
    
    function __construct($tpp = 35, $page = 0)
    {
        $this->tpp = $tpp;
        $this->page = $page;
        $this->topicsum = SO2::$DB->query('SELECT COUNT(*) FROM topics WHERE boardid = @boardid')->fetchColumn(0);
    }
    
    public function display()
    {
        // bleh
        $q = SO2::$DB->q(
                'SELECT topics.topicid, topic_title, topics.userid, messageid, mtime, '.
                       '(SELECT COUNT(*) FROM messages WHERE messages.topicid = topics.topicid) AS posts '.
                'FROM topics LEFT JOIN messages ON lastpost = messageid '.
                'WHERE boardid = @boardid '.
                'GROUP BY topicid '.
                'ORDER BY mtime DESC '.
                'LIMIT ?, ?', array($this->page*$this->tpp, $this->tpp), SO2_PDO::QOBJ);
        $topics = $q->fetchAll();
        
        echo
        "<table>\n",
        '  <col/><col/><col class="num"/><col/>',"\n",
        "  <thead>\n",
        "    <tr>\n",
        '      <th scope="col">Topic</th>',"\n",
        '      <th scope="col">Creator</th>',"\n",
        '      <th scope="col">Posts</th>',"\n",
        '      <th scope="col">Last Post</th>',"\n",
        "    </tr>\n",
        "  </thead>\n",
        "  <tbody>\n";
        
        $a = 1;
        
        foreach ( $topics as $topic ) {
            printf(
                '  <tr class="content c%d u%d">'."\n".
                '    <td><a class="topic" href="messagelist?%d">%s</a></td>'."\n".
                "    <td>%s</td>\n".
                "    <td>%d</td>\n".
                "    <td>%s</td>\n".
                "  </tr>\n",
                    (++$a&1),
                    $topic['topicid'],
                    $topic['userid'],
                    $topic['topic_title'],
                    SO2::$Page->namelink($topic['userid']),
                    $topic['posts'],
                    SO2::$Page->fdate($topic['mtime'])
            );
        }
        echo "  </tbody>\n</table>\n";
    }
    
    public function pagelist()
    {
        global $boardid; // This is a nasty hack
        $list = array();
        
        if ( $this->topicsum < $this->tpp ) {
            return null;
        }
        
        if ( 0 == $this->tpp ) {
            $this->tpp = 5;
            trigger_error(__METHOD__.': $tpp was 0, changing to 5 instead', E_USER_WARNING);
        }
        
        for ( $i = 0; $i < $this->topicsum; $i += $this->tpp ) {
            if ( $i/$this->tpp == $this->page ) {
                $list[] = '<dd class="curpage">['.($i/$this->tpp+1).']</dd>';
            } else {
                $rel = ( $i/$this->tpp == $this->page+1
                       ? 'next'
                       : ( $i/$this->tpp == $this->page-1
                         ? 'prev'
                         : '' ) );
                $list[] = sprintf('<dd%s><a href="topiclist?board=%d;page=%d;length=%d"%s>%d</a></dd>',
                                  ( $rel ? ' class="'.$rel.'"' : '' ),
                                  $boardid,
                                  ($i/$this->tpp),
                                  $this->tpp,
                                  ( $rel ? ' rel="'.$rel.'"' : '' ),
                                  ($i/$this->tpp+1)
                                 );
            }
        }
        
        return "\n\t".implode("\n\t", $list)."\n";
    }
}
?>
