<?php
/**
 * Object-oriented HTML input field with validation
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license http://specialops.ath.cx/repos/so2/trunk/COPYING (New BSD Licence)
 * @version 2.15
 */
class HTML_Input
{
    public $name;
    public $value;
    public $maxlength;
    
    function __construct($name, $value = '', $length = 20)
    {
        $this->name = $name;
        $this->value = $value;
        $this->maxlength = $length;
    }

    function check_value()
    {
        if ( ! isset($_POST[$this->name])
        or   $this->maxlength && strlen($_POST[$this->name]) > $this->maxlength ) {
            throw new OutOfBoundsException('Bad or no value given for form field "'.$this->name);
        }

    }
    
    function toString()
    {
        $field = '<input type="text"';

        foreach ( $this as $k => $v )
            if ( ! empty($k) )
                $field .= " $k=\"$v\"";

        if ( is_null($this->default) && isset($_POST[$this->name]) )
            $field .= ' value="'.$_POST[$this->name].'"';
        
        return $field.'/>';
    }
}
?>
