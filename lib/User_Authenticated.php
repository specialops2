<?php
/**
 * Class for the current logged in user.
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
class User_Authenticated extends User_Registered
{
    public $cached = false;

    function __construct($userid)
    {
        $this->sync($userid);
    }

    // Needed to stop memcache'd copy getting out of sync with the database
    function __wakeup()
    {
        if ( $this->attrcache['last_passive_date'] < T_NOW - 30
        ||   ! empty($_POST) ) {
            $this->sync($this->attrcache['userid']);
        } else {
            $this->reload();
        }
        $this->cached = true;
    }

    private function sync($userid)
    {
        parent::__construct($userid);
        
        /* Update last active thing */
        if ( $this->getopt('alwaysonline') || ! empty($_POST) ) {
            $qx = 'UPDATE LOW_PRIORITY users SET useragent = ?, last_ip = INET_ATON(?), '.
                  'last_passive_date = UNIX_TIMESTAMP(), last_active_date = last_passive_date WHERE userid = @userid';
        } else {
            $qx = 'UPDATE LOW_PRIORITY users SET useragent = ?, last_ip = INET_ATON(?), '.
                  'last_passive_date = UNIX_TIMESTAMP() WHERE userid = @userid';
        }
        SO2::$DB->q($qx, array(substr($_SERVER['HTTP_USER_AGENT'], 0, 130), $_SERVER['REMOTE_ADDR']));
        $this->reload();
    }

    private function reload()
    {
        SO2::$Page->usernav = array (
            $this->attrcache['alias'].' ('.$this->attrcache['points'].'¤)' => 'user'
        );
    }
}
?>
