<?php
require_once 'iface.Boardlist.php';

/**
 * SO2 Compact Boardlist Module
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @licence file://../COPYING
 * @version 2.15
 */
class Boardlist_Lite implements Boardlist
{
  function display()
  {
    $q = SO2::$DB->query('SELECT group_name, boardid, board_name AS name, view_lvl, points '.
                         'FROM boards LEFT JOIN board_groups USING(groupid) '.
                         'WHERE hidden = 0 ORDER BY groupid ASC, boardid ASC')->fetchAll(PDO::FETCH_ASSOC);
    
    // Really bad multidimensional array hack
    foreach ( $q as $board ) {
      $groups[array_shift($board)][array_shift($board)] = $board;
    }       
// How I mine for fish?>
<table>
<thead>
  <tr><th>Board</th><th>Posts</th><th>Topics</th><th>Last Post</th></tr>
</thead>
<?php
    foreach ( $groups as $groupname => $boards ) {
      echo '<tbody class="boardgroup">',"\n",
           '<tr><th scope="rowgroup" colspan="4">',$groupname,"</th></tr>\n";
      
      foreach ( $boards as $boardid => $board ) {
        SO2::$DB->exec('SET @boardid = '.$boardid);
        
        $counts = SO2::$DB->query('SELECT boards.posts, boards.topics, MAX(mtime) AS post FROM boards, topics, messages '.
                                  'WHERE boards.boardid = topics.boardid AND lastpost=messageid AND boards.boardid = @boardid '.
                                  'GROUP BY boards.boardid')->fetch(PDO::FETCH_ASSOC);
        
        echo '<tr class="content">',
             '<td>',(
                SO2::$User->has_access('viewboard', $board)
                ? '<a href="topiclist?'.$boardid.'">'.$board['name'].'</a>'
                : $board['name']
             ),'</td>';

        if ( $counts['posts'] ) {
          echo '<td>',$counts['posts'],'</td><td>',$counts['topics'],'</td>';
          if ( SO2::$User->has_access('viewboard', $board) ) {
            echo '<td>',SO2::$Page->fdate($counts['post']),'</td>';
          } else {
            echo '<td>N/A</td>';
          }
        } else {
          echo '<td colspan="3">N/A</td>';
        }
        echo "</tr>\n";
      }
      echo "</tbody>\n";
    }
    echo "</table>\n";
  }
}
?>
