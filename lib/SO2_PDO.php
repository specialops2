<?php
/**
 * SO2 custom PDO class. Way better than the so2mysqli one.
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
class SO2_PDO extends PDO
{
    const QSMART = PDO::FETCH_LAZY;
    const QVALUE = PDO::FETCH_COLUMN;
    const QROW   = PDO::FETCH_NUM;
    const QASSOC = PDO::FETCH_ASSOC;
    const QOBJ   = PDO::FETCH_OBJ;
    const QNONE  = null;

    /**
     * This is made of amazing.
     * @param $q      string SQL query
     * @param $params mixed  Array or single parameter for prepared statement placeholders
     * @param $fetch  int    Determines the return type. Default is auto-detection
     * @return mixed
     */
    public function q($q, $params = null, $fetch = self::QSMART)
    {
        //echo '<!--'.$q.print_r($params,1).print_r(debug_backtrace(), 1)."-->\n\n";

        if ( self::QSMART == $fetch ) {
            if ( strpos($q, 'SELECT') === 0 ) { // Default for selects is to return assoc array
                $fetch = self::QASSOC;
            } else {                              // Anything else is null
                $fetch = self::QNONE;
            }
        }

        $tmp = $this->prepare($q);
        $tmp->execute((array)$params);
        switch ( $fetch ) {
            case self::QVALUE: // Return first column
                return $tmp->fetchColumn(0);
            case self::QROW:   // Return numeric array
                return $tmp->fetch(PDO::FETCH_NUM);
            case self::QASSOC: // Return assoc array
                return $tmp->fetch(PDO::FETCH_ASSOC);
            case self::QOBJ:   // Return the result directly; can then be used with PDO's fetch*()
                return $tmp;
            case self::QNONE:  // Return nothing
                return;
            default:
                trigger_error('Unknown return type requested in SO2_PDO::q(): '.$fetch, E_USER_WARNING);
        }
    }
}
?>
