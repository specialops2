<?php
/**
 * Board helper functions
 */
class Board {
    private $id;

    function __construct($boardid) {
        $this->id = $boardid;
        SO2::$DB->q('SET @boardid = ?', $boardid);
    }

    function get_info() {
        if ( ! ($info=SO2::$Cache->get('board['.$this->id.'].info')) ) {
            $info = SO2::$DB->q('SELECT board_name, topic_lvl, post_lvl, view_lvl, points FROM boards WHERE boardid = @boardid');
            SO2::$Cache->add('board['.$this->id.'].info', $info);
        }
        return $info;
    }
}
?>
