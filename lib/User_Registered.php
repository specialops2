<?php
/**
 * Class for a registered user, but not the one viewing the page.
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
class User_Registered extends User_Anonymous
{
    public $admin;

    function __construct($userid)
    {
        $this->attrcache = SO2::$DB->q('SELECT * FROM users WHERE userid = ?', $userid);
        $this->attrcache['options'] = explode(',', $this->attrcache['options']);
        $this->admin = ('admin' == $this->attrcache['level']);
    }

    function __get($v)
    {
        switch ( $v ) {
            case 'reg_ip':
            case 'last_ip':
            case 'last_login_ip':
                return SO2::$DB->q('SELECT INET_NTOA(?)', $this->attrcache[$v], SO2_PDO::QVALUE);
            case 'password':
                return SO2::$DB->q('SELECT AES_DECRYPT(?, ?)',
                                   array($this->attrcache[$v], $this->attrcache['reg_ip']), SO2_PDO::QVALUE);
            case 'posts':
                return SO2::$DB->q('SELECT COUNT(*) FROM messages WHERE userid = ?',
                                   $this->attrcache['userid'], SO2_PDO::QVALUE);
        }

        if ( array_key_exists($v, $this->attrcache) ) {
            return $this->attrcache[$v];
        }
        
        throw new OutOfBoundsException('Invalid attribute name '.$v);
    }
    
    function __set($var, $value)
    {
        $column = '?';

        switch ( $var ) {
            case 'reg_ip':
            case 'last_ip':
            case 'last_login_ip':
                if ( strpos($value, ':') !== false ) { // IPv6
                    $value = 'NULL';
                    break;
                }
                $column = 'INET_ATON(?)';
                break;
            case 'password':
                $var = 'passwd';
                $column = 'AES_ENCRYPT(?, reg_ip)';
                break;
            case 'points':
                $this->attrcache['points'] = $value;
                $tmp = sqrt($value);

                $invites = SO2::$DB->q('SELECT COUNT(*) FROM things WHERE what = \'invite\' AND userid = ?',
                                       $this->attrcache['userid'], SO2_PDO::QVALUE);

                if ( floor($tmp) == $tmp
                &&   $tmp > $this->attrcache['invites']
                &&   $invites < 5 ) {
                    SO2::$DB->q('INSERT INTO things VALUES (NULL, ?, \'invite\', UUID())', $this->attrcache['userid']);
                    SO2::$DB->q('UPDATE users SET invites = (invites + 1) WHERE userid = ?', $this->attrcache['userid']);
                }
                break;
            case 'options':
                if ( is_array($value) ) {
                    $this->attrcache['options'] = $value;
                    $value = implode(',', $value);
                }
                break;
            default:
                $this->attrcache[$var] = $value;
        }
        SO2::$DB->q('UPDATE users SET '.$var.' = '.$column.' WHERE userid = ?', array($value, $this->attrcache['userid']));
    }
    
    function __toString()
    {
        return $this->attrcache['alias'];
    }
    
    /**
     * Check whether the user's password in the DB is Null.
     */
    public function check_nullpass()
    {
        return SO2::$DB->query('SELECT COUNT(*) FROM users WHERE userid = @userid AND passwd IS NULL')->fetchColumn(0);
    }
    
    /**
     * Access control function
     * @return mixed Value equivalent to true/false, actual value varies with implementation
     */
    public function has_access($feature, array $details = array())
    {
        if ( $this->admin )
            return 5;

        switch ( $feature ) {
            // Board access
            case 'posttopic':
                if ( $this->points < 0 || 'none' == $this->level ) return false;
                return $this->access_level($details['topic_lvl'], $details['points']);
            case 'postmessage':
                if ( 'none' == $this->level ) return false;
                return $this->access_level($details['post_lvl'], $details['points']);
            case 'viewboard':
                return $this->access_level($details['view_lvl'], $details['points']);

            // Misc stuff
            case 'allhtml':
                return ($this->attrcache['points'] >= 10000);
            case 'moderate': // Return moderator level as integer
                return ($this->attrcache['points'] > 0 ? strlen($this->attrcache['points']) - 1 : 0);

            default:
                return false;
        }
    }

    private function access_level($lev, $pts)
    {
        switch ( $lev ) {
            case 'none':
                return true;
            case 'login':
                return ('none' != $this->level); // True for all but suspended userlevel
            case 'points':
                if ( $this->attrcache['points'] >= $pts ) return true;
                // Don't return if false, check userlevel instead
            case 'vip':
                return ('vip' == $this->level);
            case 'admin': // Admin is handled at start of has_access(), never calls this function
            default:
                return false;
        }
    }

    public function getopt($name)
    {
        return in_array($name, $this->attrcache['options']);
    }
}
?>
