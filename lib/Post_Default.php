<?php
/**
 * Old & buggy PCRE-based message parser
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 * @deprecated because it sucks
 */
require_once 'lib/iface.Message3.php';

class Post_Default implements Message3
{
    const CHARSET = 'UTF-8'; // Don't change unless you know what you're doing
    const URL_DISPLAY_LENGTH = 80;
    
    private $temp_text;
    private $output = null;
    
    public static $allowed_html = array(
        /* These are the order the HTML 5 spec defines them.
           Yes I know HTML 5 is still a draft. Stop fucking whining. */
        'blockquote',
        'p',
        'pre',
        'ol', 'ul', 'li', 'dl', 'dt', 'dd',
        'em', 'strong', 'small', 'abbr', 'dfn', 'code', 'var', 'samp', 'kbd', 'sup', 'sub', 'q', 'cite',
        'ins', 'del'
    );
    
    function __construct($input, $formatting = null)
    {   
        // &-Encode everything by default
        $temp = htmlspecialchars($input);
        
        // Not allowed full HTML
        foreach ( self::$allowed_html as $tag ) {
            $preg1[] = '#&lt;('.$tag.')&gt;(.+)&lt;/('.$tag.')&gt;#Usie';
        }
        $temp = preg_replace($preg1, "'<'.strtolower('$1').'>$2</'.strtolower('$3').'>'", $temp);
        
        // Auto-link URLs
        $temp = preg_replace('#(?<=\b)(https?|ftp|irc|ut2004)://(([0-9a-zA-Z\-_.+?/%=;,~:]|&amp;)+(\#[a-zA-Z][0-9a-zA-Z_]*)?)#ie',
                            "self::makeurl('$1://$2')", $temp);
        $temp = preg_replace('#(?<=\b)(xmpp|aim):(([0-9a-zA-Z\-_.+?/%=;,~:]|&amp;)+\@[0-9a-zA-Z_.]+)#ie',
                            "self::makeurl('$1:$2')", $temp);
        
        // Add <br/>s if the no-autobr option isn't given
        if ( ! ($formatting & self::HTML_RAW) ) {
            $temp = nl2br($temp);
            
            // Trim <br/>s after block elements and other places they shouldn't be
            $temp = preg_replace('#<(/?(table|tr|[uod]l)|/(t[dh]|d[dt]|li|p|h[1-6]))>(\s*<br />)+#si',
                                '<$1>', $temp);
            // Trim <br/>s within <pre>
            $temp = preg_replace('#<pre>(.*)</pre>#Usie',
                                "'<pre>'.str_replace('<br />', '', '$1').'</pre>'", $temp);
        }
        
        if ( strpos($this->output, '<script') !== false ) {
            throw new Exception('No scripts allowed.');
        }
        
        $this->output = str_replace("\r\n", "\n", $temp);
    }
    
    private static function makeurl($uri)
    {
        $uri = html_entity_decode($uri);
        
        return '<a href="'.htmlspecialchars($uri).'" rel="external nofollow">'.( strlen($uri) > self::URL_DISPLAY_LENGTH ?
                    htmlspecialchars(substr($uri, 0, self::URL_DISPLAY_LENGTH)).'...' : htmlspecialchars($uri) ).'</a>';
    }
    
    function validate()
    {
        // Check for malformed XML, doesn't really do much else
        $parser = xml_parser_create(self::CHARSET);
        
        if ( !xml_parse($parser, '<div>'.$this->output.'</div>', true) ) {
            throw new InvalidInputException(xml_error_string(xml_get_error_code($parser)), xml_get_current_line_number($parser));
        }
    }
    
    function getOutput()
    {
        return $this->output;
    }

    function __toString()
    {
        return $this->output;
    }
}
?>
