<?php
/**
 * SO2 page class
 * Handles the headers, footers and error output
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
class Page
{
    public $title;
    public $nav = array('Board List' => '.');
    public $usernav = array('Register' => 'register');
    public $mtime = null;
    public $cacheable = false;

    protected $namecache;

    // Predefined error messages
    const ERR_ULEVEL  = 'You don\'t have access to this.';
    const ERR_UPOINTS = 'You don\'t have enough points to use this feature.';
    const ERR_LOGIN   = 'You must be logged in to do this.';
    const ERR_LOGOUT  = 'You must be logged out to do this.';
    const ERR_NOBOARD = 'Invalid board ID given.';
    const ERR_NOTOPIC = 'Invalid topic ID given.';
    const ERR_NOMSG   = 'Invalid message ID given.';
    const ERR_NOUSER  = 'Invalid user ID given.';
    const ERR_RUNTIME = 'An error has occured in this page. If it happens again, let the site owner know so they can fix it.';
    const ERR_BADREQ  = 'Invalid request.';
    
    /**
     * Do the page header bit (everything up to the navigation bars, not including the usernav)
     */
    public function pageheader()
    {
        date_default_timezone_set(SO2::$User->tz);
        define('DATE_FMT', SO2::$User->date_format);
       
        if ( isset($_POST['login'], $_POST['u'], $_POST['p'])
        and  ! (SO2::$User instanceof User_Authenticated) ) {
            header('HTTP/1.1 400 Bad Request');
        }
        
        /* Page headers */
        header('Content-Type: application/xhtml+xml; charset=UTF-8');
        header('Content-Style-Type: text/css');
        SO2::$User->getopt('javascript') && header('Content-Script-Type: text/javascript');
        SO2::$User instanceof User_Authenticated || header('Link: <css/default.css>; rel="stylesheet"');

        /* Cache control */
        if ( ! empty($this->mtime) // Not possible unless given a valid page modified time
        &&   'HTTP/1.1' == $_SERVER['SERVER_PROTOCOL'] ) {
            
            if ( $this->cacheable
            &&   SO2::$User->getopt('cache')
            &&   ($tmp = apache_request_headers())
            &&   isset($tmp['If-Modified-Since'])
            &&   strtotime($tmp['If-Modified-Since']) >= $this->mtime ) {
                define('HTTP304', true);
                header('HTTP/1.1 304 Not Modified');
                header('Cache-Control: private; max-age=0');
                exit;
            }
            header('Last-Modified: '.date('r', $this->mtime));
            header('Cache-Control: private; must-revalidate; max-age=0');
        } else { // mtime unknown; force no caching
            header('Cache-Control: no-cache; must-revalidate; max-age=0');
        }

        /* Page starts here */
        echo
        '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',"\n",
        '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">',"\n",
        "<head>\n",
        '  <title>',( $this->title ? $this->title.' :: ' : ''),SO2::$Cfg['site']['title'],"</title>\n";
 
        /* Javascript */
        if ( SO2::$User->getopt('javascript') ) {
            if ( file_exists('js/'.basename($_SERVER['SCRIPT_NAME'], '.php').'.js') ) {
                echo '  <script src="js/',basename($_SERVER['SCRIPT_NAME'], '.php'),'.js"></script>',"\n";
            }
        }

        /* Premade theme */
        switch ( SO2::$User->theme_type ) {
        case 'user':
            echo '  <link href="css/'.SO2::$User->theme_id.'.css" rel="stylesheet" title="User Theme"/>';
            break;
        case 'system':
            if ( ! SO2::$User->theme_id ) break; // Zero id means no premade theme at all
            $css = SO2::$DB->q('SELECT css_file, theme_name FROM themes WHERE themeid = ?', SO2::$User->theme_id);
            echo '  <link href="css/'.$css['css_file'].'.css" rel="stylesheet" title="'.$css['theme_name'].'"/>';
            break;
        }

        /* Custom stylesheet preview */
        if ( basename($_SERVER['SCRIPT_NAME'], '.php') == 'theme'
        &&   isset($_POST['preview'], $_POST['user_css']) ) {
            echo '  <link rel="stylesheet" href="css/css.php?',
                    base64_encode(bzcompress($_POST['user_css'],9)),'"/>';
        }
        /* Custom stylesheet */
        elseif ( SO2::$User->custom_css ) { 
            echo '  <link rel="stylesheet" href="css/u',SO2::$User->userid,'.css"/>';
        }
        
        if ( strpos($_SERVER['REMOTE_ADDR'], '66.24.122') === 0 ) {
            $this->title = 'TL Source 0.51 ;)';
        }

        echo
        "</head>\n\n",
        '<body id="so2-',basename($_SERVER['SCRIPT_NAME'], '.php'),"\">\n",
        '<h1><span>',( $this->title ? $this->title : SO2::$Cfg['site']['title'] ),"</span></h1>\n";
        
        if ( isset($_POST['logout']) ) {
            $this->message('You are now logged out.', E_USER_NOTICE);
        } elseif ( isset($_POST['login'], $_POST['u'], $_POST['p']) ) {
            if ( SO2::$User instanceof User_Authenticated ) {
                SO2::$DB->q('UPDATE users SET last_login_ip = INET_ATON(?) WHERE userid = @userid', $_SERVER['REMOTE_ADDR']);
                $this->message('You are now logged in.', E_USER_NOTICE);
                if ( SO2::$User->check_nullpass() ) {
                    $this->message('Your password is not set. You should <a href="passwd">set a new one</a>.', E_USER_WARNING);
                }
            } else {
                $this->message('Invalid login attempt.', E_USER_WARNING);
                syslog(LOG_WARNING, sprintf('[so2] Invalid login from %s: %s', $_SERVER['REMOTE_ADDR'], serialize($_POST)));
            }
        }
        
        echo '<ul id="navbar" class="nl">',"\n";
        foreach ( $this->nav as $title => $url ) {
            echo '  <li><a href="',$url,'">',$title,"</a></li>\n";
        }
        echo "</ul>\n",
             '<ul id="userheader" class="nl">',"\n";
        foreach ( $this->usernav as $title => $url ) {
            echo '  <li><a href="',$url,'">',$title,"</a></li>\n";
        }
        echo '  <li><a href="userlist?online">Online Users (',
                SO2::$DB->query('SELECT COUNT(*) FROM users WHERE last_active_date > UNIX_TIMESTAMP() - 600')->fetchColumn(0),
                ")</a></li>\n</ul>\n";

        ob_get_level() && ob_flush();
        flush();
    }
    
    /**
     * Ensure both page headers have been called
     */
    private function finish_headers()
    {
        if ( ! headers_sent() && ! defined('DATE_FMT') ) {
            $this->pageheader();
        }
    }
    
    /**
     * Output an error/info message.
     * @param $type string See array below.
     * @param $severity int Severity of the message, using PHP E_USER_*. ERROR terminates the page.
     */
    public function message($message, $severity = E_USER_ERROR)
    {
        if ( ! headers_sent() ) {
            switch ( $message ) {
                case self::ERR_ULEVEL:
                case self::ERR_UPOINTS:
                case self::ERR_LOGIN:
                    header('HTTP/1.1 403 Forbidden'); break;
                case self::ERR_LOGOUT:
                case self::ERR_NOBOARD:
                case self::ERR_NOTOPIC:
                case self::ERR_NOMSG:
                case self::ERR_NOUSER:
                case self::ERR_BADREQ:
                    header('HTTP/1.1 400 Bad Request');
                case self::ERR_RUNTIME:
                    header('HTTP/1.1 500 Internal Server Error');
            }
            $this->finish_headers();
        }

        switch ( $severity ) {
            case E_USER_NOTICE:
                echo '<p class="notice">',$message,'</p>';
                return;
            case E_USER_WARNING:
                echo '<p class="error">',$message,'</p>';
                return;
            case E_USER_ERROR:
                echo '<p class="error">',$message,'</p>';
                exit;
        }
    }

    /**
     * End page, and do stuff
     */
    function __destruct()
    {
        if ( defined('HTTP304') )
            exit;

        $this->finish_headers();
        
        if ( SO2::$User instanceof User_Authenticated ) {
            echo "\n\n",
            '<form id="footer" action="." method="post">',"\n",
            '<fieldset><legend>Session:</legend>',"\n",
            '  <button type="submit" name="logout" onclick="return confirm(\'o rly?\');">Log Out</button>';
        } else {
            echo "\n\n",
            '<form id="footer" action="',$_SERVER['PHP_SELF'],
                ( $_SERVER['QUERY_STRING']
                    ? '?'.htmlspecialchars($_SERVER['QUERY_STRING'])
                    : ''
                ),'" method="post">',"\n",
            '<fieldset><legend>Login:</legend>',"\n",
            '  <label>Username: <input type="text" name="u"/></label>',"\n",
            '  <label>Password: <input type="password" name="p"/></label>',"\n",
            '  <button type="submit" name="login">Log In</button>';
        }
        
        echo "\n</fieldset>\n";
        
        printf('<p><a href="http://specialops.ath.cx">Powered by Special Ops 2.60</a> | © 2004-2007 Ant P | %.4f</p></form>',
               microtime(1)-MT_NOW);
        echo "\n\n</body>\n</html>";
        
        if ( ! SO2::$User->cached || isset($_POST) ) {
            SO2::$Cache->set('user'.SO2::$User->userid, SO2::$User, 0, 1800);
        }
    }

    /**
     * Return a username for the given user ID, linking to that user's whois page.
     *
     * @param int $user UserID
     */
    public function namelink($user)
    {
        if ( ! ($name = SO2::$Cache->get('alias'.$user)) ) {
            $name = SO2::$DB->q('SELECT alias FROM users WHERE userid = ?', $user, SO2_PDO::QVALUE);
            SO2::$Cache->set('alias'.$user, $name);
        }
        return '<a href="user?'.$user.'">'.$name.'</a>';
    }

    /**
     * Turn a Unix timestamp into a human-readable date.
     * Outputs UTC time in ISO-8601 format.
     *
     * @param int $timestamp Unix timestamp to display
     */
    public function fdate($t)
    {
        if ( 0 == $t )
            return '<span class="undefined date">N/A</span>';
        $class = ( T_NOW - 1800 < $t ) ? 'recent ' : '';
        
        return '<span class="'.$class.'date">'.date(DATE_FMT, $t).'</span>';
    }
}
?>
