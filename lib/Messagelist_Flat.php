<?php
/**
 * Plain messagelist like GF uses
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
require_once 'lib/iface.Messagelist.php';

class Messagelist_Flat implements Messagelist
{
    protected $postcount;
    protected $msgs;
    protected $args;

    protected $topicid;
    protected $mo;
    protected $mpp;
    protected $page;
    
    
    function __construct($topicid, Messagestyle $mo, $mpp, $page)
    {
        if ( 0 == $mpp ) {
            throw new Exception('<blink>CANNOT DIVIDE BY ZERO</blink>');
        }

        $this->msgs = SO2::$DB->q('SELECT userid, mtime, topicid, replyto, score, marks, messageid, INET_NTOA(origin_ip) AS ip '.
                                  'FROM messages WHERE topicid = ? ORDER BY mtime ASC LIMIT ?, ?',
                                  array($topicid, $mpp*$page, $mpp), SO2_PDO::QOBJ)->fetchAll(PDO::FETCH_ASSOC);
        $this->postcount = SO2::$DB->q('SELECT COUNT(*) FROM messages WHERE topicid = ?', $topicid, SO2_PDO::QVALUE);

        list($this->topicid, $this->mo, $this->mpp, $this->page) = func_get_args();
    }
    
    public function display()
    {
        foreach ( $this->msgs as $m ) {
            if ( $m['score'] < SO2::$User->cutoff && SO2::$User->userid != $m['userid'] )
                $m['mtext'] = sprintf(self::MODMSG, SO2::$User->cutoff);
            $this->mo->display($m);
        }
    }
    
    public function pagelist()
    {
        // Not enough to fill one page
        if ( $this->postcount < $this->mpp ) {
            return null;
        }
        
        $list = range(0, $this->postcount/$this->mpp);
        $str = '';

        foreach ( $list as $pagenum ) {
            switch ( $pagenum ) {
                case $this->page-1: // current page in script is off by one from display here
                    $str .=  sprintf('<dd class="prev"><a href="messagelist?topic=%d;page=%d;length=%d" rel="prev">%d</a></dd>',
                                     $this->topicid, $pagenum, $this->mpp, $pagenum+1); break;
                case $this->page:
                    $str .= '<dd class="curpage">'.($pagenum+1).'</dd>'; break;
                case $this->page+1:
                    $str .=  sprintf('<dd class="next"><a href="messagelist?topic=%d;page=%d;length=%d" rel="next">%d</a></dd>',
                                     $this->topicid, $pagenum, $this->mpp, $pagenum+1); break;
                default:
                    $str .=  sprintf('<dd><a href="messagelist?topic=%d;page=%d;length=%d">%d</a></dd>',
                                     $this->topicid, $pagenum, $this->mpp, $pagenum+1); break;
                
            }
        }
        
        return $str;
    }
}
?>
