<?php
/**
 * Boardlist class interface
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
interface Boardlist
{
    /**
     * Echo the board list. Doesn't return anything. Using anything within the SO2 namespace is allowed.
     * @return null
     */
    public function display();
}
?>
