<?php
/**
 * Message display interface
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
interface Messagestyle
{
    /**
     * Take a message and output it.
     * @param array $message Message to echo
     */
    public function display(array $message);
}
?>
