<?php
/**
 * XML message parser - doesn't do anything other than an XML well-formedness check
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
require_once 'lib/iface.Message3.php';

class Post_XML implements Message3
{
    private $output = null;
    
    function __construct($input, $formatting = null)
    {
        if ( strpos($input, '<script') !== false || strpos($input, '-moz-binding') !== false ) {
            throw new Exception('Your post contains something that looks like HTML scripting.');
        }
        
        // No nl2br here, anyone using this should know what they're doing
        $this->output = str_replace("\r\n", "\n", $input);
    }
    
    function validate()
    {
        // Check for malformed XML, doesn't really do much else
        $parser = xml_parser_create('UTF-8');
        
        if ( !xml_parse($parser, '<div>'.$this->output.'</div>', true) ) {
            throw new InvalidInputException(xml_error_string(xml_get_error_code($parser)), xml_get_current_line_number($parser));
        }
    }
    
    function getOutput()
    {
        return $this->output;
    }
}
?>
