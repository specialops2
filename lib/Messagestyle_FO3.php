<?php
/**
 * FO3 style messagelist
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
require_once 'lib/Messagestyle_Default.php';

class Messagestyle_FO3 extends Messagestyle_Default
{
    static $lastquoteid;
    static $quotes = array();

    protected function msgbody(array $m)
    {
        if ( $m['replyto'] && $m['replyto'] != self::$lastmsgid && $m['replyto'] != self::$lastquoteid ) {
            // This bit makes sure it works across multiple pages
            if ( ! isset(self::$quotes[$m['replyto']]) ) {
                self::$quotes[$m['replyto']] = SO2::$DB->q('SELECT userid, score, topicid, messageid '.
                                                           'FROM messages WHERE messageid = ?', $m['replyto']);
    
                if ( self::$quotes[$m['replyto']]['score'] < SO2::$User->cutoff
                &&   SO2::$User->userid != self::$quotes[$m['replyto']]['userid'] ) {
                    self::$quotes[$m['replyto']]['mtext'] = '<span class="deletedmsg">[...]</span>';
                }
            }

            
            // This makes no sense.
            echo '<fieldset class="u',self::$quotes[$m['replyto']]['userid'],"\">\n",
                 '  <legend>Quote ',SO2::$Page->namelink(self::$quotes[$m['replyto']]['userid']),
                   '; post <a href="detail?',$m['replyto'],'">#',$m['replyto'],"</a></legend>\n",
                 '  <blockquote><div>',parent::msgbody(self::$quotes[$m['replyto']]),"</div></blockquote>\n",
                 "</fieldset>\n";
        }

        self::$lastquoteid = $m['replyto'];

        parent::msgbody($m);
    }
}
?>
