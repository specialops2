<?php
/**
 * Dummy memcache class
 * Used for fallback when normal memcache is not available for whatever reason
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
class Memcache {
    function __call($a, $b)
    {
        return false;
    }
}
