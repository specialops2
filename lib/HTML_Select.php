<?php
/**
 * Object-oriented HTML selectbox with validation
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license http://specialops.ath.cx/repos/so2/trunk/COPYING (New BSD Licence)
 * @version 2.15
 */
class HTML_Select implements Countable
{
    public $name;
    public $indent;
    public $default;
    public $attributes;
    private $values = array();
    
    function __construct($name, $indent = 0, $default = null, $attributes = array())
    {
        $this->indent = $indent;
        $this->default = $default;
        $this->attributes = $attributes;
        $this->attributes['name'] = $name;
    }
    
    function add_item($value, $name = '')
    {
        if ( !$name ) {
            $name = $value;
        }
        $this->values[$name] = $value;
    }
    
    function array_fill(array $items)
    {
        foreach ( $items as $row ) {
            if ( is_array($row) ) {
                $this->add_item($row[0], $row[1]);
            } else {
                $this->add_item($row);
            }
        }
    }
    
    function set_default($value)
    {
        $this->default = $value;
    }
    
    function check_value(&$value)
    {
        if ( !isset($value) || !in_array($value, $this->values) ) {
            throw new OutOfBoundsException('Bad value given for selectbox "'.$this->attributes['name'].'": '.$value);
        }
    }
    
    function __toString()
    {
        $t = str_repeat("  ", $this->indent);
        
        // Concat attributes into a string
        $attribs = '';
        foreach ( $this->attributes as $name => $value ) {
            $attribs .= sprintf(' %s="%s"', $name, $value);
        }
        
        // Concat options into another string (how exciting!)
        $opts = '';

        foreach ( $this->values as $name => $value ) {
            $opts .= sprintf('%s  <option value="%s"%s>%s</option>'."\n",
                             $t, $value, ( $value == $this->default ? ' selected="selected"' : '' ), $name);
        }
        return "\n$t<select".$attribs.">\n".$opts.$t."</select>\n$t";
    }

    public function count()
    {
        return count($this->values);
    }
}
?>
