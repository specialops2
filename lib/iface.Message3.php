<?php
/**
 * Message filter module interface.
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @version 3.0
 * @license http://specialops.ath.cx/repos/so2/trunk/COPYING
 */
interface Message3 {
//  /* Do normal XHTML checking and nl2br (default) */
    const HTML_FILTER = 0x0;
    /* Don't automatically add linebreaks */
    const HTML_RAW    = 0x1;
    // Plaintext or full-html stuff should be handled by the filter module
    
    /**
     * Constructor
     *
     * @param string $input HTML string to check, usually a $_POST value
     * @param int $formatting Formatting flags to use
     */
    function __construct($input, $formatting = null);
    
    /**
     * Check input syntax, if invalid throw an InvalidInputException saying why.
     */
    function validate();
    
    /**
     * Get processed input back.
     *
     * @return null|string Null if an unrecoverable error occured
     */
    function getOutput();
}
?>
