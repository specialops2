<?php
/**
 * User interface
 *
 * Stuff that the $user object needs to support.
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.50
 */
interface User
{
    /**
     * Access control check
     *
     * @param string $feature The thing to check for
     * @param array $details Any external info that needs to be passed (usually board data from SQL)
     * @return bool True if they have it, false otherwise
     */
    public function has_access($feature, array $details = array());


    /**
     * Does exactly what it looks like.
     */
    public function getopt($name);
    
    /**
     * Should return the username, or whatever's appropriate
     */
    public function __toString();
}
?>
