<?php
/**
 * HTML parser interface
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @version 4.0
 * @license http://specialops.ath.cx/repos/so2/trunk/COPYING
 */
interface HTMLParser {
    /* Flags to be passed in the $options parameter */
    const BREAKS = 0x1; // add <br/> or paragraphs as appropriate
    const ESCAPE = 0x2; // Convert all HTML tags into entities
    const STRICT = 0x4; // Attempt to validate HTML
    const REPAIR = 0x8; // Attempt to correct broken HTML before doing anything else

    /**
     * Constructor
     *
     * @param string $input HTML string to check, usually a $_POST value
     * @param int $formatting Formatting flags to use
     */
    function __construct($input, $options);
}
?>
