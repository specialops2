<?php
/**
 * DTD-based HTML filter.
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 0
 */
require_once 'lib/iface.Message3.php';

abstract class Message_DTD implements Message3 {
    
    private $DOM;
    
    function __construct($input, $formatting = null)
    {
        $this->DOM = new DOMDocument;
    }
}
?>
