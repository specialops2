<?php
/**
 * Threaded messagelist layout
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://../COPYING
 * @version 2.15
 */
require_once 'lib/iface.Messagelist.php';

class Messagelist_Threaded implements Messagelist
{
    protected $tree;
    protected $topicid;
    protected $mo;
    
    function __construct($topicid, Messagestyle $mo, $mpp, $page)
    {
        $tmp = SO2::$DB->q('SELECT userid, mtime, topicid, replyto, score, marks, messageid, INET_NTOA(origin_ip) AS ip '.
                           'FROM messages WHERE topicid = ?', $topicid, SO2_PDO::QOBJ);
        while ( $item = $tmp->fetch(PDO::FETCH_ASSOC) ) {
            $this->tree[$item['replyto']][] = $item;
        }

        list($this->topicid, $this->mo) = func_get_args();
    }
    
    public function display()
    {
        $this->printtree(NULL);
    }
    
    private function printtree($branch)
    {
        foreach ( $this->tree[$branch] as $m ) {
            if ( $m['score'] < SO2::$User->cutoff && SO2::$User->userid != $m['userid'] )
                $m['mtext'] = sprintf(self::MODMSG, SO2::$User->cutoff);
            $this->mo->display($m);
            
            if ( ! empty($this->tree[$m['messageid']]) ) {
                echo '<div class="thread">',"\n";
                    $this->printtree($m['messageid']);
                echo "</div>\n";
            }
        }
    }
    
    // Not implemented
    public function pagelist()
    {
        return null;
    }
}
?>
