<?php
// This file has terrible indenting
require_once 'iface.Boardlist.php';

/**
 * SO2 Default Boardlist Module
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @licence file://../COPYING
 * @version 2.15
 */
class Boardlist_Default implements Boardlist
{
  function display()
  {
    $q = SO2::$DB->query('SELECT group_name, boardid, board_name AS name, caption, view_lvl, points '.
                         'FROM boards LEFT JOIN board_groups USING(groupid) '.
                         'WHERE hidden = 0 ORDER BY groupid ASC, boardid ASC')->fetchAll(PDO::FETCH_ASSOC);
    
    // Really bad multidimensional array hack
    foreach ( $q as $board ) {
      $groups[array_shift($board)][array_shift($board)] = $board;
    }
?>
<table>
<col/><col class="num"/><col class="num"/><col/>
<thead>
<tr>
  <th scope="col">Board</th>
  <th scope="col">Posts</th>
  <th scope="col">Topics</th>
  <th scope="col">Last Post</th>
</tr>
</thead>
<?php
    foreach ( $groups as $groupname => $boards ) {
      echo '<tbody class="boardgroup">',"\n",
           '<tr><th scope="rowgroup" colspan="4">',$groupname,"</th></tr>\n";
      
      foreach ( $boards as $boardid => $board ) {
        SO2::$DB->exec('SET @boardid = '.$boardid);
        
        $counts = SO2::$DB->query('SELECT boards.posts, boards.topics, MAX(lastpost) AS lastpost '.
                                  'FROM boards LEFT JOIN topics USING(boardid) '.
                                  'WHERE boardid = @boardid GROUP BY boardid')->fetch(PDO::FETCH_ASSOC);
        
        echo '<tr class="content">',"\n",
             '  <td>'.( SO2::$User->has_access('viewboard', $board)
                      ? '<a href="topiclist?'.$boardid.'">'.$board['name'].'</a>'
                      : $board['name']
                      ).
             '<br/><small>',$board['caption'],"</small></td>\n";
        
        // If no posts then skip the last three columns completely
        if ( $counts['posts'] ) {
          $p = SO2::$DB->q('SELECT messages.userid, mtime, topics.topicid, topic_title, topics.userid AS tposter '.
                           'FROM messages LEFT JOIN topics USING(topicid) '.
                           'WHERE messageid = ?', $counts['lastpost']);
          
          echo '  <td>',$counts['posts'],"</td>\n",
               '  <td>',$counts['topics'],"</td>\n";
          
          if ( SO2::$User->has_access('viewboard', $board) ) {
            echo "  <td><small>\n",
                 '  From: ',SO2::$Page->namelink($p['userid']),
                    ' at ',SO2::$Page->fdate($p['mtime']),"<br/>\n",
                 '  in: <a class="topic u',$p['tposter'],'" href="messagelist?',$p['topicid'],'#m',$counts['lastpost'],'">',
                        $p['topic_title'],"</a></small></td>\n";
          } else {
            echo "  <td>N/A</td>\n";
          }
        } else {
          echo "  <td colspan=\"3\">No Posts</td>\n";
        }
        echo "</tr>\n";
      }
      echo "</tbody>\n";
    }
    echo "</table>\n";
  }
}
?>
