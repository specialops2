<?php
/**
 * HTML checkbox object
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license http://specialops.ath.cx/repos/so2/trunk/COPYING (New BSD Licence)
 * @version 2.15
 */
class HTML_Checkbox
{
    public $name;
    public $label;
    public $value;
    
    function __construct($name, $label)
    {
        $this->name = $name;
        $this->label = $label;
        $this->value = isset($_POST[$this->name]);
    }

    function is_selected()
    {
        return isset($_POST[$this->name]);
    }

    function __toString()
    {
        return '<label>'.
               '<input type="checkbox" name="'.$this->name.'"'.($this->value ? ' checked="checked"' : '').'/>'.
               htmlspecialchars($this->label).'</label>';
    }

    static function factory(array $boxes)
    {
        $ret = array();
        foreach ( $boxes as $name => $label ) {
            $ret[$name] = new HTML_Checkbox($name, $label);
        }
        return $ret;
    }
}
?>
