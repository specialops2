<?php
/**
 * User Account Registration
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';
SO2::$Page->title = 'Account Registration';

if ( SO2::$User instanceof User_Authenticated ) {
    isset($_POST['login'])
    ? (header('Location: http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/') && exit)
    : SO2::$Page->message('You\'re already registered!');
}

if ( isset($_POST['prompt']) ) {
    $_GET = $_POST;
}

$uri = new HTML_Select('uri', 3);
$uri->add_item('mailto:', 'E-Mail');
$uri->add_item('xmpp:', 'Jabber/GTalk');
$uri->add_item('aim:', 'AIM');
$uri->add_item('http://');
$uri->add_item('irc://');
$uri->add_item('', 'Other (specify)');
$uri->set_default('mailto:');

if ( isset($_POST['something']) ) {
    
    // Data validation
    try {
        // Referrer check
        if ( !isset($_SERVER['HTTP_REFERER']) || !preg_match('/register/', $_SERVER['HTTP_REFERER']) ||
                strpos($_SERVER['HTTP_REFERER'], 'http://'.$_SERVER['HTTP_HOST']) !== 0 ) {
            throw new InvalidInputException('Invalid HTTP referrer sent.');
        }

        // Check address
        $uri->check_value($_POST['uri']);
        if ( empty($_POST['uri']) && !preg_match('/^[a-zA-Z0-9]+:/', $_POST['addr']) ) {
            throw new InvalidInputException('Contact method not specified.');
        }
        
        // Set $address if valid
        if ( 'Other (specify)' == $_POST['uri'] ) {
            $address = $_POST['addr'];
        } else {
            $address = $_POST['uri'].$_POST['addr'];
        }
        
        // Blank fields
        if ( empty($_POST['reg_u']) || empty($_POST['reg_p']) || empty($_POST['addr']) ) {
            throw new InvalidInputException('You left one or more fields empty.');
        }
        
        // Mismatched passwords
        if ( $_POST['reg_p'] !== $_POST['reg_c'] ) {
            throw new InvalidInputException('Both passwords must match exactly.');
        }
        
        // Username
        $_POST['reg_u'] = trim($_POST['reg_u']);
        if ( preg_match('/\s{2,}|[<>&]|[Pp]ojr/', $_POST['reg_u']) ) {
            throw new InvalidInputException('Username contains invalid characters.');
        }
        
        // Username in use
        if ( SO2::$DB->q('SELECT COUNT(*) FROM users WHERE alias = ?', $_POST['reg_u'], SO2_PDO::QVALUE) ) {
            throw new InvalidInputException('That username is already in use. Try a different name.');
        }
        
        // Invite system
        if ( isset($_GET['code'])
        &&   isset($_GET['user'])
        &&   SO2::$DB->q('SELECT COUNT(*) FROM things WHERE what = \'invite\' AND data = ? AND userid = ?',
                         array($_GET['code'], $_GET['user']), SO2_PDO::QVALUE) ) {
            define('INVITED', true);
        }

        if ( defined('INVITE_ONLY') && !defined('INVITED') ) {
            sleep(7);
            throw new InvalidInputException('Form data was submitted incorrectly.');
        }
        
        // Flood protection
        if ( SO2::$DB->q('SELECT COUNT(*) FROM users WHERE reg_ip = INET_ATON(?)', $_SERVER['REMOTE_ADDR'], SO2_PDO::QVALUE) ) {
            throw new RateLimitException(
                'You can only register one account. If you lost your password, contact an admin'.
                ' and a new one will be sent to the URI you specified when you signed up.');
        }
        
        SO2::$DB->beginTransaction();
        
        SO2::$DB->q('SET @userip = INET_ATON(?)', $_SERVER['REMOTE_ADDR']);
        
        // Retard protection
        if ( empty($_POST['CYA']) ) {
            header('HTTP/1.1 403 Forbidden');
            SO2::$DB->exec('SET @userpass = \'banned\'');
        } else {
            header('HTTP/1.1 202 Accepted');
            SO2::$DB->q('SET @userpass = AES_ENCRYPT(?, @userip)', $_POST['reg_p']);
        }
        
        SO2::$DB->q('INSERT INTO users ('.
                        'alias, referrer, reg_contact, points, passwd, last_login_ip, reg_ip, register_date, last_active_date'.
                    ') VALUES (?, ?, ?, ?, @userpass, @userip, @userip, UNIX_TIMESTAMP(), UNIX_TIMESTAMP() )',
            array( $_POST['reg_u'], (defined('INVITED') ? $_GET['user'] : null), $address, intval(defined('INVITED')) )
        );
        
        $u = SO2::$DB->lastInsertId();
        SO2::$DB->q('SET @userid = ?', $u);
        SO2::$User = new User_Authenticated($u);
        
        if ( defined('INVITED') ) {
            SO2::$DB->q('DELETE FROM things WHERE what = \'invite\' AND data = ? AND userid = ?',
                        array($_GET['code'], $_GET['user']) );
        }
        
        setcookie('u', $_POST['reg_u'], T_NOW+86400);
        setcookie('p', $_POST['reg_p'], T_NOW+86400);
        
        SO2::$Page->pageheader();
        
        if ( empty($_POST['CYA']) ) {
            echo '<p class="error">TOS Rejected - Account Banned.</p>';
        } else {
            echo '<p class="info">Your account has been created!</p>';
        }
        
        SO2::$DB->commit();
        exit;
        
    } catch ( InvalidInputException $e ) {
        header('HTTP/1.1 400 Bad Request');
        SO2::$Page->pageheader();
        echo '<p class="error">',$e->getMessage(),'</p>';
    } catch ( RateLimitException $e ) {
        header('HTTP/1.1 400 Bad Request');
        SO2::$Page->pageheader();
        echo '<p class="error">',$e->getMessage(),'</p>';
    }

} elseif ( defined('INVITE_ONLY')
      && ! ( isset($_GET['code'])
          && isset($_GET['user'])
          && SO2::$DB->q('SELECT COUNT(*) FROM things NATURAL LEFT JOIN users '.
                         'WHERE what = \'invite\' AND data = ? AND users.userid IS NOT NULL AND things.userid = ?',
                         array($_GET['code'], $_GET['user']), SO2_PDO::QVALUE)
           )
         ) {
    SO2::$Page->pageheader();
?>

<p class="error">You need a valid invitation code to create an account.</p>
<p>Enter the registration code you were given and the user ID number of the person who gave you it.</p>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="get">
  <table class="inputlist">
    <tr><th scope="row">Code</th><td><input type="text" name="code" size="36" maxlength="36"/></td></tr>
    <tr><th scope="row">User ID</th><td><input type="text" name="user" size="5"/></td></tr>
  </table>
  <p><button type="submit">Confirm</button></p>
  <p>Don't have an invite? One of our members might.</p>
</form>

<?php
    exit;
} else {
    SO2::$Page->pageheader();
}


if ( ip2long($_SERVER['REMOTE_ADDR']) === false ) { // ipv6 doesn't work yet
    echo '<p class="error">Error: You have to register from an IPv4 address.</p>',"\n";
    exit;
}

if ( isset($_GET['user'], $_GET['code']) ) {
    printf('<form action="%s?user=%d;code=%s" method="post">',
            $_SERVER['PHP_SELF'], intval($_GET['user']), htmlentities($_GET['code']));
} else {
    echo '<form action="',$_SERVER['PHP_SELF'],'" method="post">';
}

$q = SO2::$DB->query('SELECT COLUMN_NAME, CHARACTER_MAXIMUM_LENGTH '.
                     'FROM information_schema.COLUMNS '.
                     'WHERE TABLE_SCHEMA = \''.SO2::$Cfg['db']['name'].'\' '.
                     'AND TABLE_NAME = \'users\' '.
                     'AND COLUMN_NAME IN(\'alias\', \'passwd\', \'reg_contact\')');
while ( $row = $q->fetch() ) {
    $limits[$row[0]] = $row[1];
}
$q = null;

function field($which=null, $range=null)
{
    if ( $which && !empty($_POST[$which]) ) {
        if ( is_numeric($range) ) {
            echo ' maxlength="',$range,'" value="',htmlspecialchars($_POST[$which]),'"';
        } elseif ( true === $range ) {
            echo ' checked="checked"';
        }
    } elseif ( is_numeric($range) ) {
        echo ' maxlength="',$range,'"';
    }
}
?>

<fieldset><legend>Register Account</legend>
  <p>All fields are required.</p>
  <table class="inputlist">
    <tr>
      <th scope="row">Username <small>(max <?php echo $limits['alias'] ?> characters)</small></th>
      <td><input type="text" name="reg_u"<?php field('reg_u', $limits['alias']) ?>/></td>
    </tr>
    <tr>
      <th scope="row">Password</th>
      <td><input type="password" name="reg_p"<?php field(null, $limits['passwd']) ?>/></td>
    </tr>
    <tr>
      <th scope="row">Confirm Password</th>
      <td><input type="password" name="reg_c"<?php field(null, $limits['passwd']) ?>/></td>
    </tr>
    <tr>
      <th scope="row">Contact URI</th>
      <td><?php echo $uri ?><input type="text" name="addr"<?php field('addr', $limits['reg_contact']-10) ?>/></td>
    </tr>
  </table>

<?php readfile('res/tos.xml'); ?>

  <p><label>
      <input type="checkbox" name="CYA"<?php field('CYA', true); ?>/>
      I have read and agree to follow the board rules.
  </label></p>
  <p>Enter the CAPTCHA: <input type="text" name="captcha"/><br/><img src="/stuff/magiceye.jpg" alt="Enter these letters"/></p>
  <p>Reading the <a href="stuff#faq">FAQ</a> is also advised.</p>
  <p><button type="submit" name="something">Create Account</button></p>
</fieldset>
</form>
