<?php
/**
 * Message/Topic posting page
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';

SO2::$Page->title = 'Post Message';


// Various post limits
define('MSG_MIN_LENGTH', 2);
define('MSG_MAX_LENGTH', 262144);
define('TOPIC_MIN_LENGTH', 2);
define('TOPIC_MAX_LENGTH',
        SO2::$DB->query('SELECT CHARACTER_MAXIMUM_LENGTH FROM information_schema.COLUMNS '.
                        'WHERE TABLE_SCHEMA = "'.SO2::$Cfg['db']['name'].'" AND TABLE_NAME = "topics" '.
                        'AND COLUMN_NAME = "topic_title"')->fetchColumn(0) );


// If messageid specified, validate it then set topic+boardid from that
if ( isset($_GET['message']) ) {
    $messageid = intval($_GET['message']);
    $topicid = SO2::$DB->q('SELECT @topicid := topicid FROM messages WHERE messageid = ?', $messageid, SO2_PDO::QVALUE);
    
    if ( ! $topicid ) {
        SO2::$Page->message(Page::ERR_NOMSG);
    }
    
    $topic = SO2::$DB->query('SELECT topic_title, boardid FROM topics WHERE topicid = @topicid')->fetch(PDO::FETCH_NUM);
    
    if ( ! is_array($topic) ) {
        SO2::$Page->message(Page::ERR_NOTOPIC);
    }
    
    $boardid = $topic[1];
} else {
    $boardid = intval($_GET['board']);
}


// Get board metadata
$board = SO2::$DB->q('SELECT board_name, view_lvl, topic_lvl, post_lvl, points, @boardid := boardid AS boardid '.
                     'FROM boards WHERE boardid = ?', $boardid);

if ( ! $board ) { // Check whether board exists
    SO2::$Page->message(Page::ERR_NOBOARD);
}

if ( ! SO2::$User->has_access('viewboard', $board) ) { // Check whether user can view board
    SO2::$Page->message(Page::ERR_ULEVEL);
}

SO2::$Page->nav['Topic List: '.$board['board_name']] = 'topiclist?'.$boardid; // Add link to board if they can view it
if ( isset($topicid) ) { // Add link to topic if replying to one
    SO2::$Page->nav['Message List: '.$topic[0]] = 'messagelist?'.$topicid;
}

// Access control
if ( isset($_GET['board']) && ! SO2::$User->has_access('posttopic', $board) ) { // Check if posting topic
    SO2::$Page->message(Page::ERR_ULEVEL);
} elseif ( ! SO2::$User->has_access('postmessage', $board) ) { // Check if replying to topic
    SO2::$Page->message(Page::ERR_ULEVEL);
}


/**
 * Selectbox for HTML formatting
 */
$html_options = new HTML_Select('html', 3);
if ( SO2::$User->has_access('allhtml') ) {
    $html_options->add_item('Post_XML', 'Full XHTML');
}
if ( 0 < SO2::$User->points ) {
    $html_options->add_item('Post_Default', 'Default');
}
$html_options->add_item('Post_Plaintext', 'Plaintext');

// Used for message preview
$mo = new Messagestyle_Default;

$score = ceil(SO2::$User->points/300); // Default score is points/300 rounded up: 1 point for 1-299, 2 for 300-599 etc.
if ( SO2::$User->posts == 0 ) // New users get one point
    $score = 1;
if ( SO2::$User->points < 0 ) // Negative score users get penalised propotional to the magnitude of their points
    $score = -(strlen(SO2::$User->points)-1);

$tmp = SO2::$DB->query('SELECT COUNT(*) FROM messages WHERE userid = @userid AND mtime > UNIX_TIMESTAMP()-60')->fetchColumn(0);
if ( $tmp == 4 ) { // 4 posts in 1 minute is more than you could possibly have a legit use for - flood
    if ( SO2::$User->points > 0 ) // If their points are above 0, halve them
        SO2::$User->points >>= 1;
    $score = -10; // Take 10 off 
} elseif ( $tmp > 4 ) { // User really is flooding - fuck them over
    $_POST['logout'] = 'Log Out';
    setcookie('u', '-', time());
    setcookie('p', '-', time());
    SO2::$Page->message(Page::ERR_LOGIN);
}

// Needed to make permalinks in message previews not cause an error
define('HERE', $_SERVER['REQUEST_URI']);

// Form submit handling
if ( isset($_POST['post']) || isset($_POST['preview']) ) {
    
    if ( isset($_POST['html']) ) {
        $html_options->check_value($_POST['html']);
        $html_options->set_default($_POST['html']);
    } else {
        $html_options->set_default('Post_'.SO2::$User->post_html);
    }
    
    try {
        // Checks done on message
        $message = new $html_options->default(trim($_POST['message_text']));
        $message->validate(); // Throws InvalidMessageException

        if ( strlen($message->getOutput()) < MSG_MIN_LENGTH ) {
            throw new LengthException('Your message is %d character(s) too short.',
                                      MSG_MIN_LENGTH - strlen($message->getOutput()) );
        }
        
        // Checks done on topic title if posting a topic
        if ( ! isset($topic) ) {
            $topic_title = trim( SO2::$User->admin ? $_POST['topic_title'] : htmlspecialchars($_POST['topic_title']) );
            $t = strlen($topic_title);
            if ( $t < TOPIC_MIN_LENGTH ) {
                throw new LengthException('Your topic title is %d character(s) too short.', TOPIC_MIN_LENGTH - $t);
            }
            if ( $t > TOPIC_MAX_LENGTH ) {
                throw new LengthException('Your topic title is %d character(s) too long.', $t - TOPIC_MAX_LENGTH);
            }
            if ( preg_match('/\S{30,}/', $topic_title) ) {
                throw new LengthException('harbl');
            }
            if ( SO2::$DB->q('SELECT COUNT(*) FROM topics WHERE topic_title = ? AND boardid = @boardid',
                             $topic_title, SO2_PDO::QVALUE) ) {
                throw new RateLimitException('A topic with that name already exists.');
            }
        }
        
        if ( isset($_POST['post']) ) {
            if ( ! (SO2::$User instanceof User_Authenticated) ) {
                throw new Exception('Login warning in plain sight ignored. You are officially retarded.');
            }
            
            SO2::$DB->beginTransaction();

            // Insert topic data if posting a topic
            if ( ! isset($topic) ) {
                SO2::$DB->q('INSERT INTO topics (topic_title, boardid, userid) VALUES (?, @boardid, @userid)', $topic_title);
                $topicid = SO2::$DB->q('SELECT @topicid := ?', SO2::$DB->lastInsertId(), SO2_PDO::QVALUE);
                mkdir('data/messages/'.$topicid);

                SO2::$User->points += ($score+1);
                $messageid = null;
            } else {
                assert($messageid);
                SO2::$User->points += $score;
            }

            // Message metadata
            SO2::$DB->q('INSERT INTO messages (topicid, replyto, userid, mtime, origin_ip, score) '.
                        'VALUES (?, ?, @userid, UNIX_TIMESTAMP(), INET_ATON(?), ? )',
                        array($topicid, $messageid, $_SERVER['REMOTE_ADDR'], $score) );
            $replyid = SO2::$DB->lastInsertId();
            
            // The message text
            file_put_contents("data/messages/$topicid/$replyid", $message->getOutput());
            
            SO2::$DB->commit();
            
            if ( isset($topicid) ) {
                $r = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/messagelist?'.$topicid.'#m'.$replyid;
            } else {
                $r = 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/topiclist?'.$boardid;
            }
            
            header('HTTP/1.1 303 See Other');
            header('Location: '.$r);
            SO2::$Page->message('Message posted. You should be redirected, if not <a href="'.$r.'">click here.</a>.',
                                E_USER_NOTICE);
            exit;
            
        } elseif ( isset($_POST['preview']) ) {
            SO2::$Page->pageheader();
            echo '<fieldset class="',get_class($mo),'"><legend>Message Preview</legend>',"\n",
                    ( isset($topic_title) ? '<h2>'.$topic_title."</h2>\n" : '' );
            $mo->display(array(
                'userid' => SO2::$User->userid,
                'mtime' => T_NOW,
                'mtext' => $message->getOutput(),
                'replyto' => null,
                'score' => $score,
                'marks' => 0,
                'messageid' => null,
                'ip' => $_SERVER['REMOTE_ADDR']
            ));
            echo "</fieldset>\n";
        }
    } catch ( LengthException $e ) {
        SO2::$Page->message(sprintf($e->getMessage(), $e->getCode()), E_USER_WARNING);
    } catch ( RateLimitException $e ) {
        SO2::$Page->message(sprintf($e->getMessage(), $e->getCode()), E_USER_WARNING);
    } catch ( InvalidInputException $e ) {
        SO2::$Page->message('Your message contains formatting errors (only the first error is shown): '.
                       $e->getMessage().' at line '.$e->getCode(), E_USER_WARNING);
    }
} else {
    SO2::$Page->pageheader();
}

// I have no idea what's going on here
$message = strpos($_SERVER['HTTP_USER_AGENT'], 'KHTML') ? "\n" : '';
if ( ! empty($_POST['message_text']) ) {
    $message = htmlspecialchars($_POST['message_text']);
} elseif ( SO2::$User->sig ) {
    $message .= "\n".htmlspecialchars(SO2::$User->sig);
}

if ( isset($messageid) ) {
    echo '<fieldset class="',get_class($mo),'"><legend>Replying to:</legend>',"\n";
    $mo->display(SO2::$DB->q('SELECT userid, mtime, topicid, replyto, score, marks, messageid, INET_NTOA(origin_ip) AS ip '.
                             'FROM messages WHERE messageid = ?', $messageid));
    echo "</fieldset>\n",
         '<form action="post?message=',$messageid,'" method="post">';
} else {
    echo '<form action="post?board=',$boardid,'" method="post">',"\n",
         '  <fieldset><legend>Topic <small>(Max. ',TOPIC_MAX_LENGTH," chars)</small></legend>\n",
         '    <input type="text" name="topic_title" maxlength="',TOPIC_MAX_LENGTH,'" size="80"',
              ( !empty($topic_title) ? ' value="'.htmlspecialchars($_POST['topic_title']).'"' : '' ),' tabindex="1"/>',"\n",
         "  </fieldset>\n";
}
?>
  <fieldset><legend>Message <small>(Max. size <?php echo (int)MSG_MAX_LENGTH/1024 ?>kiB)</small></legend>
    <textarea rows="15" cols="60" name="message_text" id="messagebox" tabindex="1"><?php echo $message ?></textarea>
    <fieldset class="content">
<?php if ( ! (SO2::$User instanceof User_Authenticated) ) {
      SO2::$Page->message(Page::ERR_LOGIN, E_USER_WARNING); ?>
      <p><label>Username: <input name="u" tabindex="1" type="text"/></label></p>
      <p><label>Password: <input name="p" tabindex="1" type="password"/></label></p>
      <input type="hidden" name="login" value="post"/>
<?php } ?>
      <button type="submit" accesskey="p" tabindex="1" name="post">Post (P)</button>
      <button type="submit" accesskey="r" tabindex="1" name="preview">Preview (R)</button>
      <p><label>HTML Formatting: <?php echo $html_options ?></label></p>
      <p>Allowed HTML in default mode: <?php echo implode(', ', Post_Default::$allowed_html) ?></p>
    </fieldset>
  </fieldset>
</form>
