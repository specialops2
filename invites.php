<?php
/**
 * User Invites and Points FAQ
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';

SO2::$Page->title = 'Invites and Points Status';

if ( ! (SO2::$User instanceof User_Authenticated) ) {
    SO2::$Page->message(Page::ERR_LOGIN);
}
SO2::$Page->pageheader();

echo "<h2>User Invites</h2>\n";

define('REG_URI', 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI']).'/register.php');

$q = SO2::$DB->query('SELECT data FROM things WHERE what = \'invite\' AND userid = @userid')->fetchAll(PDO::FETCH_COLUMN);
if ( count($q) ) {
    echo "<ul>\n";
    foreach ( $q as $code ) {
        printf('  <li><a href="'.REG_URI.'?user=%d;code=%s">'.REG_URI.'?user=%1$d;code=%2$s</a></li>'."\n",
               SO2::$User->userid, $code);
    }
    echo "</ul>\n<p>Users registering through one of these links start off with 1 point instead of 0. You can have a maximum of 5 invites at any one time.</p>\n";
} else {
    echo "<p>You have no invites at the moment.\n</p>";
}

$q = SO2::$DB->query('SELECT userid FROM users WHERE referrer = @userid')->fetchAll(PDO::FETCH_COLUMN);
if ( count($q) ) {
    echo "<h3>Invited Users:</h3>\n<ul>\n";
    foreach ( $q as $tmp ) {
        echo '  <li>',SO2::$Page->namelink($tmp),"</li>\n";
    }
    echo "</ul>\n";
}
for ( $i = max(SO2::$User->points + 1, pow(SO2::$User->invites, 2)); floor(sqrt($i)) != sqrt($i); $i++ );
?>

<h2>Points</h2>
<p>You have <?php echo SO2::$User->points ?> points. Points to next invite: <?php echo $i - SO2::$User->points ?>.</p>
