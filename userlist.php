<?php
/**
 * User Directory/Online Users/Top 50 page
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';

$headers = array (
    'ID' => array('userid', 0),
    'Username' => array('alias', 0),
    'Points' => array('points', 1),
    'Posts' => array('posts', 1),
    'Last Active' => array('last_active_date', 1),
    'Idle Time' => null
);
$sort = array('ASC', 'DESC');

// DO NOT WANT
if ( isset($_GET['sort']) && in_array($_GET['sort'], array_keys($headers)) ) {
    $field = ' ORDER BY '.$headers[$_GET['sort']][0].' '.$sort[ $headers[$_GET['sort']][1]^isset($_GET['rev']) ];
} else {
    $field = null;
}

if ( isset($_GET['online']) ) {
    SO2::$Page->title = 'Currently Online Users';
    $where = 'WHERE last_active_date > UNIX_TIMESTAMP() - 600 GROUP BY userid ASC'.$field;
} else {
    if ( 0 >= SO2::$User->points ) {
        SO2::$Page->message(Page::ERR_UPOINTS);
    }

    SO2::$Page->title = 'User List';
    $where = 'GROUP BY userid ASC'.$field;

    if ( SO2::$DB->query('SELECT COUNT(*) FROM users')->fetchColumn(0) > 50 ) {
        SO2::$Page->title = 'Top 50 Users';
        $where .= ' LIMIT 50'.( $field ? $field : ' ORDER BY points DESC');
    }
}

SO2::$Page->pageheader();

define('HERE', 'userlist?'.(isset($_GET['online']) ? 'online;' : ''));
?>

<table id="userlist">
  <thead>
    <tr>
      <?php foreach ( $headers as $label => $field ) {
          echo '      ';
          if ( $field ) {
              $rev = ( isset($_GET['sort']) && !isset($_GET['rev']) && $_GET['sort'] == $label ) ? ';rev">↑' : '">↓';
              echo '<th><a href="',HERE,'sort=',urlencode($label),$rev,$label,'</a></th>';
          } else {
              echo '<th>',$label,'</th>';
          }
      } ?>
    </tr>
  </thead>
  <tbody>
<?php
    $q = SO2::$DB->query('SELECT users.userid, points, last_active_date, COUNT(messageid) AS posts '.
                         'FROM users NATURAL LEFT JOIN messages '.$where)->fetchAll(PDO::FETCH_ASSOC);
    
    function idle($last)
    {
        // voodoo
        $idle = T_NOW - $last;
        $idle -= ($time[2] = $idle % 60); $idle /= 60;
        $idle -= ($time[1] = $idle % 60); $idle /= 60;
        $idle -= ($time[0] = $idle % 24); $time[3] = $idle / 24;
        
        ksort($time);
        return ( $time[3] ? $time[3].' day'.( 1 != $time[3] ? 's ' : ' ' ) : '').vsprintf('%d:%02d:%02d', $time);
    }
    
    $a = 1;
    
    foreach ( $q as $row ) {
        // eww
        printf("<tr class='content c%d'>\n".
               "  <td>%d</td><td>%s</td><td>%d</td><td>%d</td><td>%s</td><td>%s</td>\n".
               "</tr>\n",
               (++$a&1), $row['userid'], SO2::$Page->namelink($row['userid']), $row['points'],
               $row['posts'], SO2::$Page->fdate($row['last_active_date']), idle($row['last_active_date'])
        );
    }
  ?>
  </tbody>
</table>
