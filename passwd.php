<?php
/**
 * User Password Editor
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';
SO2::$Page->title = 'Change Password';

if ( ! (SO2::$User instanceof User_Authenticated) ) {
    SO2::$Page->message(Page::ERR_LOGIN);
}

if ( isset($_POST['submit']) ) {
    try {
        if ( ! SO2::$User->check_nullpass()
        &&   strval(SO2::$User->password) != $_POST['old'] ) {
            throw new InvalidInputException('Old password does not match.');
        }
        if ( $_POST['new'] != $_POST['n2'] ) {
            throw new InvalidInputException('New password not confirmed correctly.');
        }
        if ( ! strlen(trim($_POST['new'])) ) {
            throw new InvalidInputException('New password must not be blank.');
        }
        
        SO2::$User->password = $_POST['new'];
        setcookie('p', $_POST['new'], T_NOW+172800);
        
        SO2::$Page->message('Password changed.', E_USER_NOTICE);
    } catch ( InvalidInputException $e ) {
        SO2::$Page->message($e->getMessage(), E_USER_WARNING);
    }
} else {
    SO2::$Page->pageheader();
}
?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
<table>
  <tbody>
    <tr>
      <th scope="row"><label for="old">Old password</label></th>
      <td><input type="password" name="old" id="old"<?php echo SO2::$User->check_nullpass() ? ' disabled="disabled"' : '' ?>/></td>
    </tr>
    <tr>
      <th scope="row"><label for="new">New password</label></th>
      <td><input type="password" name="new" id="new"/></td>
    </tr>
    <tr>
      <th scope="row"><label for="n2">Retype password</label></th>
      <td><input type="password" name="n2" id="n2"/></td>
    </tr>
  </tbody>
</table>
<p><button type="submit" name="submit">Confirm</button></p>
</form>
