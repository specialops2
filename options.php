<?php
/**
 * User Settings Editor
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';
SO2::$Page->title = 'Options Screen';

if ( ! (SO2::$User instanceof User_Authenticated) ) {
    SO2::$Page->message(Page::ERR_LOGIN);
}

SO2::$Page->usernav['Theme Settings'] = 'theme';

// Set field size limits
$q = SO2::$DB->query('SELECT COLUMN_NAME , CHARACTER_MAXIMUM_LENGTH '.
                     'FROM information_schema.COLUMNS '.
                     'WHERE TABLE_SCHEMA = "'.SO2::$Cfg['db']['name'].'" '.
                     'AND TABLE_NAME = "users" '.
                     'AND CHARACTER_MAXIMUM_LENGTH IS NOT NULL');
while ( $row = $q->fetch(PDO::FETCH_NUM) ) {
    $limits[$row[0]] = $row[1];
}
$q = null;

// function to make the SO2::$User->options checkboxes
function makeoptbox($n, $label, $title)
{
    return "<label for='$n'><input type='checkbox' name='options[$n]' id='$n'".
           ( SO2::$User->getopt($n) ? ' checked="checked"' : '' ).
           "/> $label<br/>\n<small>$title</small></label><br/>\n";
}

// function to make input textfields
function maketextbox($n, $l)
{
    return '<input type="text" name="'.$n.'" value="'.SO2::$User->$n.'" maxlength="'.$l.'" size="'.min(30, $l).'"/>';
}

// make the {topic,message,board}list selectboxes
$selects = array(
    'Messagelist' => 'msglist_layout',
    'Messagestyle' => 'msglist_style',
    'Topiclist' => 'topiclist_layout',
    'Boardlist' => 'boardlist_layout',
    'Post' => 'post_html'
);

foreach ( $selects as $class => $field ) {
    ${$field} = new HTML_Select($field, 4, SO2::$User->$field);
    
    foreach ( glob('lib/'.$class.'_*.php') as $filename ) {
        include_once $filename;
        
        preg_match('/_(.*?)\.php/', $filename, $tmp);
        ${$field}->add_item($tmp[1]);
    }
}

// Timezone selectbox
$tz = new HTML_Select('tz', 2, SO2::$User->tz);
$tz->array_fill(timezone_identifiers_list());

// Form submittal
if ( isset($_POST['submit']) ) {
    try {
        /**
         * Check whether a $_POST value exists, is a number, and is between the minimum/maximum
         */
        function rangecheck($var, $min, $max, $name)
        {
            if ( !isset($_POST[$var]) || !is_numeric($_POST[$var]) ||
                $_POST[$var] < $min || $_POST[$var] > $max ) {
                throw new OutOfBoundsException(sprintf('%s must be a number between %d and %d.', $name, $min, $max));
            }
            return intval($_POST[$var]);
        }
        
        /**
         * Check whether a $_POST value exists and its string length is below the limit defined for it
         */
        function lengthcheck($var, $name)
        {
            global $limits;
            if ( !isset($_POST[$var]) || strlen($_POST[$var]) > $limits[$var] ) {
                throw new LengthException(sprintf('%s can\'t be longer than %d characters.', $name, $limits[$var]));
            }
            return $_POST[$var];
        }
        
        // PHP really needs a coerce function
        if ( empty($_POST['options']) ) {
            $_POST['options'] = array();
        }
        
        // Fields to check/update
        $numbers = array (
            'msgs_page'   => 'Messages per page',
            'topics_page' => 'Topics per page'
        );
        $strings = array (
            'sig'             => 'Signature',
            'quote'           => 'Quote',
            'date_format'     => 'Custom date format',
            'public_contact'  => 'Public contact address',
            'private_contact' => 'Private contact address'
        );
        
        SO2::$DB->beginTransaction();
        
        // Validation and database updating:

        // Select boxes
        foreach ( $selects as $field ) {
            ${$field}->check_value($_POST[$field]);
            ${$field}->default = SO2::$User->$field = $_POST[$field];
        }
        $tz->check_value($_POST['tz']);
        $tz->default = SO2::$User->tz = $_POST['tz'];
        
        // Number values
        foreach ( $numbers as $varname => $displayname ) {
            SO2::$User->$varname = rangecheck($varname, 5, 100, $displayname);
        }
        SO2::$User->cutoff = rangecheck('cutoff', -999, 999, 'Post Threshold');


        // Strings
        foreach ( $strings as $varname => $displayname ) {
            SO2::$User->$varname = lengthcheck($varname, $displayname);
        }
        if ( htmlspecialchars($_POST['date_format']) != $_POST['date_format'] ) {
            throw new OutOfBoundsException('Date format cannot contain special HTML characters.');
        }
        
        // Options
        SO2::$User->options = array_intersect( array_keys($_POST['options']),
                            array('alwaysonline', 'javascript', 'cache', 'quickpost') );
        
        SO2::$DB->commit();
        
        SO2::$Page->message('Settings have been saved.', E_USER_NOTICE);
    } catch ( OutOfBoundsException $e ) {
        SO2::$Page->message($e->getMessage(), E_USER_WARNING);
    } catch ( LengthException $e ) {
        SO2::$Page->message($e->getMessage(), E_USER_WARNING);
    }
} else {
    SO2::$Page->pageheader();
}

$options['Display Settings'] = array (
    'Message list layout' => array('info'  => 'Threaded view disables messages per page setting',
                                   'field' => $msglist_layout.$msglist_style ),
    'Topic list layout' => array('field' => $topiclist_layout ),
    'Board list layout' => array('field' => $boardlist_layout ),
    'Messages per page' => array('info'  => 'Between 5-100',
                                 'field' => maketextbox('msgs_page', 3) ),
    'Topics per page' => array('info'  => 'Between 5-100',
                               'field' => maketextbox('topics_page', 3) ),
    'Timezone' => array('field' => $tz.': '.strip_tags(SO2::$Page->fdate(T_NOW)) ),
    'Time format' => array('info' => 'See PHP <a href="//php.net/date">date</a> documentation. Default: "Y-m-d H:i:s"',
                           'field' => maketextbox('date_format', $limits['date_format']) ),
);
$options['Miscellaneous'] = array (
    'Post threshold' => array('info' => 'Posts below this many points will be hidden from the message list.',
                              'field' => maketextbox('cutoff', 4) ),
    'Initial post HTML method' => array('field' => $post_html ),
    'Other Options' => array('field' => '<fieldset>'.
                        makeoptbox('alwaysonline', 'Enable active time updating',
                                   'Disabling this will only update your last active time when you post.').
                        makeoptbox('javascript', 'Enable Javascript',
                                   'Enables optional JS/AJAX functionality.').
                        makeoptbox('cache', 'Low bandwidth mode',
                                   'Activates HTTP caching. Breaks most pages.').
                        makeoptbox('quickpost', 'Use quickpost box',
                                   'Puts a quick post box under the topic and message list.').
                        '</fieldset>'
                       )
);

$options['Profile'] = array (
    'Signature' => array('info' => 'Appended to the bottom of your posts.',
                         'field' => '<textarea rows="4" cols="60" name="sig">'.htmlspecialchars(SO2::$User->sig).'</textarea>'),
    'Quote' => array('info' => 'Shown in your userinfo page. Use plain text only.',
                     'field' => '<textarea rows="4" cols="60" name="quote">'.htmlspecialchars(SO2::$User->quote).'</textarea>'),
    'Public contact address' => array('info' => 'Only visible to logged in users.',
                                      'field' => maketextbox('public_contact', $limits['public_contact']) ),
    'Private contact address' => array('info' => 'Only visible to you and admins.',
                                       'field' => maketextbox('private_contact', $limits['private_contact']) )
);
?>

<form method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
<?php
foreach ( $options as $title => $fields ) { ?>
<fieldset><legend><?php echo $title ?></legend>
<table id="<?php strtr(strtolower($title), ' ', '_') ?>">
  <thead>
    <tr><th scope="col">Option</th><th scope="col">Value</th></tr>
  </thead>
  <tbody>
<?php
    $a = 1;
    foreach ( $fields as $name => $stuff ) { ?>
    <tr class="content c<?php echo ++$a&1 ?>">
      <td><?php echo $name ?></td>
      <td><?php
        echo $stuff['field'];
        if ( isset($stuff['info']) )
            echo ' <small>',$stuff['info'],'</small>';
       ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
</fieldset>
<?php } ?>
<p><input type="submit" name="submit" value="Save Settings (S)" accesskey="s"/></p>
</form>
