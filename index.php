<?php
/**
 * Board List
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';

// HTTP 1.1 cache setup
SO2::$Page->mtime = SO2::$DB->query('SELECT GREATEST( MAX(messages.mtime), MAX(users.last_active_date) ) '.
                                    'FROM messages, users')->fetchColumn(0);
SO2::$Page->cacheable = true;
SO2::$Page->nav = array('Homepage' => '/', 'Stats/FAQ' => 'stuff');
SO2::$Page->pageheader();
$activeusers = SO2::$DB->query('SELECT userid FROM users '.
                               'WHERE last_active_date > UNIX_TIMESTAMP() - 600 '.
                               ( SO2::$User->admin ? 'OR last_passive_date > UNIX_TIMESTAMP() - 600 ' : '' ).
                               'ORDER BY last_active_date ASC LIMIT 30')->fetchAll(PDO::FETCH_COLUMN);
?>

<div id="boardlist-stuff">
<?php readfile('res/announcement.xml') ?>
<hr/>
<p>Page load time: <?php echo SO2::$Page->fdate(T_NOW) ?></p>
<p><?php
    switch ( count($activeusers) ) {
        case 0: echo 'There are 0 registered users online.'; break;
        case 1: echo 'There is one registered user online: ',SO2::$Page->namelink($activeusers[0]); break;
        default: echo 'Currently online registered users: ',implode(', ', array_map(array(SO2::$Page, 'namelink'), $activeusers));
    }
?></p>
</div>

<?php
if ( file_exists('lib/Boardlist_'.SO2::$User->boardlist_layout.'.php') ) {
    $tmp = 'Boardlist_'.SO2::$User->boardlist_layout;
    $bl = new $tmp;
} else {
    SO2::$User->boardlist_layout = 'Default';
    $bl = new Boardlist_Default;
}

echo '<div id="boardlist" class="',get_class($bl),"\">\n";
    $bl->display();
echo "</div>\n";
?>
