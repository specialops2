<?php
/**
 * Theme Editor
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @license file://COPYING
 * @version 2.15
 */

require 'con.php';
SO2::$Page->title = 'Theme Settings';

if ( ! (SO2::$User instanceof User_Authenticated) ) {
    SO2::$Page->message(Page::ERR_LOGIN);
}
SO2::$Page->usernav['General Options'] = 'options';

$advancedmode = HTML_Checkbox::factory(array(
    'enable' => 'Use Advanced Theme',
    'append' => 'Additive (add after the selected basic theme)'
));
$premademode = HTML_Radiobutton::factory('themetype', array(
    'system' => 'Premade theme: ',
    'user' => 'Another user\'s custom theme: '
), (isset($_POST['themetype']) ? $_POST['themetype'] : SO2::$User->theme_type));

$importpremade = new HTML_Checkbox('importpre', 'Import basic theme to advanced');

$premade_themes = new HTML_Select('premade_theme');
$user_themes = new HTML_Select('user_theme');
if ( 'system' == SO2::$User->theme_type ) {
    $premade_themes->default = SO2::$User->theme_id;
} else {
    $user_themes->default = SO2::$User->theme_id;
}

$premade_themes->add_item(0, '(No CSS)');
$premade_themes->array_fill(SO2::$DB->query('SELECT themeid, theme_name FROM themes ORDER BY theme_name ASC')->fetchAll());

if ( isset($_POST['save']) ) {
    SO2::$DB->beginTransaction();

    // Do stuff for premade theme
    if ( $premademode['system']->is_selected() ) {
        $premade_themes->check_value($_POST['premade_theme']);
        $premade_themes->default = intval($_POST['premade_theme']);
        
        if ( $importpremade->value && intval($_POST['premade_theme']) != 0 ) {
            $filename = SO2::$DB->q('SELECT css_file FROM themes WHERE themeid = ?', $_POST['premade_theme'],
                                    SO2_PDO::QVALUE).'.css';
        } else {
            SO2::$User->theme_type = 'system';
            SO2::$User->theme_id = intval($_POST['premade_theme']);
        }
    }
    // Do stuff for user-made theme
    elseif ( $premademode['user']->is_selected() ) {
        $user_themes->default = intval($_POST['user_theme']);

        if ( $importpremade->value && file_exists('css/'.$filename) ) {
            $filename = "u{$_POST['user_theme']}.css";
        } else {
            SO2::$User->theme_type = 'user';
            SO2::$User->theme_id = intval($_POST['user_theme']);
        }
    }
    
    // Import premade theme
    if ( $importpremade->is_selected() && isset($filename) ) {
        copy('css/'.$filename, 'css/u'.SO2::$User->userid.'.css');
        $advancedmode['enable']->value = true;
        $advancedmode['append']->value = false;
    }
    // Save CSS if advanced mode is enabled
    elseif ( $advancedmode['enable']->is_selected() ) {
        file_put_contents('css/u'.SO2::$User->userid.'.css', str_replace("\r\n", "\n", $_POST['user_css']));
    }
    // Clean up empty files
    elseif ( empty($_POST['css']) && file_exists('css/u'.SO2::$User->userid.'.css') ) {
        unlink('css/u'.SO2::$User->userid.'.css');
    }
    
    // Update user settings
    if ( $advancedmode['enable']->is_selected() && ! $advancedmode['append']->is_selected() ) {
        SO2::$User->theme_type = 'system';
        SO2::$User->theme_id = 0;
    }
    SO2::$User->custom_css = $advancedmode['enable']->value;
    $importpremade->value = false;

    SO2::$DB->commit();
    
    SO2::$Page->message('Settings have been saved.', E_USER_NOTICE);
} elseif ( isset($_POST['preview']) ) {
    SO2::$Page->message('This is a preview of your custom theme. Changes have not been saved.', E_USER_NOTICE);
} else {
    SO2::$Page->pageheader();
}
$user_themes->array_fill(SO2::$DB->query('SELECT userid, alias FROM users WHERE custom_css = 1 AND theme_type = "system" '.
                                         'AND theme_id = 0 ORDER BY alias ASC')->fetchAll());
?>

<form id="theme" method="post" action="<?php echo $_SERVER['PHP_SELF'] ?>">
  <fieldset class="content">
    <legend>Basic Themes</legend>
    <p><?php echo $premademode['system'], $premade_themes ?></p>
    <p><?php if ( count($user_themes) ) echo $premademode['user'], $user_themes ?></p>
    <p><?php echo $importpremade ?></p>
  </fieldset>
  
  <p><button type="submit" name="save" accesskey="s">Save Settings (S)</button></p>
  
  <fieldset class="content">
    <legend><?php echo $advancedmode['enable'] ?></legend>
    <div id="advanced">
      <p><?php echo $advancedmode['append'] ?></p>
      <fieldset>
        <legend>Edit CSS</legend>
        <textarea rows="20" cols="80" name="user_css"><?php
            if ( isset($_POST['preview'], $_POST['user_css']) )
                echo htmlspecialchars($_POST['user_css']);
            elseif ( file_exists('css/u'.SO2::$User->userid.'.css') )
                echo htmlspecialchars(file_get_contents('css/u'.SO2::$User->userid.'.css'));
        ?></textarea>
        <p><a href="css/">CSS Directory</a> | <button type="submit" name="preview" accesskey="p">Preview (P)</button></p>
      </fieldset>

      <h3>CSS examples</h3>
      <ul>
        <li>Make page title right-aligned: <code>h1 { text-align: right }</code></li>
        <li>Increase the font size of the board list: <code>#so2-index { font-size: 1.1em }</code></li>
        <li>Lock the navigation menus to the left of the screen: <code>.nl { position: fixed; left: 0 }</code></li>
        <li>Make the messagelist fixed width: <code>#messagelist { max-width: 60em }</code></li>
        <li>Decrease the indent on threaded messagelists: <code>.Messagelist_Threaded .thread { margin-left: 0.2em }</code></li>
        <li>Give IRC layout a monospace font: <code>.Messagelist_Flat.Messagestyle_IRC { font-family: monospace }</code></li>
        <li>Highlight all errors, notices and info messages: <code>.error, .notice, .info { color: #f00 }</code></li>
        <li>Give alternating table rows different colours: <code>.c0 { background: #fff } .c1 { background: #ddd }</code></li>
        <li>Hide threads below a certain depth until clicked on:
        <pre>.thread .thread .message > .content { display: none } .message:target > .content { display: block }</pre></li>
        <li>Block anything posted by the user with userid #24: <code>.u24 { display: none !important }</code></li>
      </ul>
    </div>
  </fieldset>
</form>

