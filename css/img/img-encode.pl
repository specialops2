#!/usr/bin/perl
# Takes image filename, outputs file contents in format for css background
# (Public Domain) / Ant P. / 2007-02-01
use strict;
use warnings;
use URI::data;

foreach (@ARGV) {
    print &file_to_data($_)."\n\n" if ( -r );
}

sub file_to_data {
    my $file = shift;
    (my $type = `file -bi $file`) =~ s/\s//g;
    my $dat = URI->new('data:');

    print "========== $file\n";
    if ( $type eq 'image/png' ) {
        print `optipng -i0 $file`;
    } else {
        print "Not preprocessing filetype $type\n";
    }
    
    $dat->media_type($type);
    $dat->data(scalar(`cat $file`));
    
    return $dat;
}
