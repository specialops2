<?php
/**
 * con.php: Generic setup file.
 *
 * Contains user authentication, database connection and other exciting stuff.
 * All the configuration stuff is done in res/config.ini now. Edit that, not this.
 *
 * @author Ant P <p@cpi.merseine.nu>
 * @licence file://COPYING
 * @version 2.50
 */

define('MT_NOW', microtime(1)); // Used for page load time stats
define('T_NOW', time()); // Used to save calling time() everywhere
set_time_limit(4); // Prevent infinite loop stuff

// PHP version check
if ( version_compare(PHP_VERSION, '5.1', '<') ) {
    include 'res/server-error.inc';
    throw new Exception('PHP 5.1 or higher is required to run SO2.');
}

// Preload required classes - autoload seems slow for some reason
require 'lib/Page.php';
require 'lib/SO2_PDO.php';
require 'lib/User_Anonymous.php';
function __autoload($classname)
{
    require 'lib/'.$classname.'.php';
}
// Utility classes for form-handling stuff
class InvalidInputException extends Exception {}
class RateLimitException    extends Exception {}


// Namespace container for important bits
class SO2 {
    static $DB;
    static $Page;
    static $User;
    static $Cfg;
    static $Cache;
}

// Page header/footer/error handling/utility functions (date display/user links)
SO2::$Page = new Page;

// Config
SO2::$Cfg = parse_ini_file('res/config.ini', true);
if ( isset(SO2::$Cfg['site']['closed']) ) {
    define('INVITE_ONLY', true);
}

// Memcache
SO2::$Cache = new Memcache;
SO2::$Cache->pconnect('localhost');

// MySQL
try {
    SO2::$DB = new SO2_PDO('mysql:dbname='.SO2::$Cfg['db']['name'], SO2::$Cfg['db']['uname'], SO2::$Cfg['db']['passwd'],
                           array(PDO::ATTR_PERSISTENT => true, PDO::ATTR_EMULATE_PREPARES => false) );
    SO2::$DB->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    SO2::$Cfg['db']['passwd'] = null;
} catch ( PDOException $e ) {
    include 'res/server-error.inc';
    throw new Exception('Database connection died');
}


// Error handling
if ( defined('DEVELOPER') ) {
    ob_start();
    function e_handler($exception)
    {
        header('HTTP/1.1 500 Internal Server Error');
        header('Content-Type: text/html; charset=UTF-8');
        echo "<h1 onmouseover='blank_stare()'>OMFG!!1</h1>\n",
             '<pre class="error">',$exception,'</pre>';
        error_log($exception);
        exit;
    }
} else {
    function e_handler($exception)
    {
        if ( ! headers_sent() ) {
            header('HTTP/1.1 500 Internal Server Error');
            header('Content-Type: text/html; charset=UTF-8');
        }
        error_log($exception);
        SO2::$Page->message(Page::ERR_RUNTIME);
    }
}
set_exception_handler('e_handler');

// Destroy cookies on logout
if ( isset($_POST['logout']) ) {
    setcookie('u', '', 100);
    setcookie('p', '', 100);
    unset($_COOKIE);
}
// Set cookies on login
elseif ( isset($_POST['login'], $_POST['u'], $_POST['p']) ) {
    list($_COOKIE['u'], $_COOKIE['p']) = array($_POST['u'], $_POST['p']);
}

// User authentication
if ( isset($_COOKIE['u'], $_COOKIE['p']) ) {
    $userid = SO2::$DB->q('SELECT @userid := userid FROM users WHERE alias = ? '.
                          ' AND (passwd = AES_ENCRYPT(?, reg_ip) OR passwd IS NULL)',
                          array($_COOKIE['u'], $_COOKIE['p']), SO2_PDO::QVALUE);
    // Logged in
    if ( $userid ) {
        setcookie('u', $_COOKIE['u'], T_NOW+172800);
        setcookie('p', $_COOKIE['p'], T_NOW+172800);
        
        require 'lib/User_Registered.php';
        require 'lib/User_Authenticated.php';
        SO2::$User = SO2::$Cache->get('user'.$userid)
        or SO2::$User = new User_Authenticated($userid);
    }
    // Failed login attempt
    else {
        setcookie('u', '', 1);
        setcookie('p', '', 1);
        unset($_COOKIE);
        
        SO2::$User = new User_Anonymous;
    }
}
// Not logged in
else SO2::$User = new User_Anonymous;
?>
